package org.apache.hadoop.mapred;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class ResumeTaskAction extends TaskTrackerAction {
  final TaskAttemptID taskId;
  
  public ResumeTaskAction() {
    super(ActionType.RESUME_TASK);
    taskId = new TaskAttemptID();
  }
  
  public ResumeTaskAction(TaskAttemptID taskId) {
    super(ActionType.RESUME_TASK);
    this.taskId = taskId;
  }

  public TaskAttemptID getTaskID() {
    return taskId;
  }
  
  @Override
  public void write(DataOutput out) throws IOException {
    super.write(out);
    taskId.write(out);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    super.readFields(in);
    taskId.readFields(in);
  }
}
