import argparse
import math
import os
import sys

def mk_argparser():
    p = argparse.ArgumentParser()
    p.add_argument('-m','--min-maps',help='filter jobs that have num maps '
                                        + 'smaller than this')
    p.add_argument('-r','--min-reduces',help='filter jobs that have num '
                                        + 'reduces smaller than this')
    p.add_argument('-s','--reducer-size',help='amount of data that each '
                                            + 'reducer handles')
    p.add_argument('-o','--original-cluster-size',help='number of machines '
                                                 + 'in the original cluster')
    p.add_argument('-c','--cluster-size',help='number of machines in the new '
                                            + 'cluster'
                       ,required=True)
    p.add_argument('--reduce-slots',default=2,type=int)
    p.add_argument('infile',type=argparse.FileType('rt'))
    return p

class SWIMJob(object):
    def __init__(self,jid,arrival_time,num_blocks,shuffle_size,output_size
                     ,num_reduce_slots
                     ,block_size=64.*1024*1024
                     ,map_speed=1.*1024*1024
                     ,reduce_speed=1.*1024*1024
                     ,data_per_reducer=sys.maxint):
        self.jid = jid
        self.arrival_time = arrival_time
        self.num_blocks = num_blocks
        self.block_size = block_size
        self.shuffle_size = shuffle_size
        self.output_size = output_size
        self.map_speed = map_speed
        self.reduce_speed = reduce_speed
        self.data_per_reducer = data_per_reducer
        ideal_num_reducers = long(math.ceil(float(self.shuffle_size)/
                                                  self.data_per_reducer))

        if ideal_num_reducers <= num_reduce_slots:
            self.num_reducers = ideal_num_reducers
        else:
            self.num_reducers = num_reduce_slots

    def get_num_mappers(self):
        return self.num_blocks
    
    def get_num_reducers(self):
        return self.num_reducers

    def get_mapper_duration(self):
        return long(math.floor((self.block_size/self.map_speed)*1000.))

    def get_reducer_duration(self):
        if self.shuffle_size == 0:
            return 0
        single_reduce_size = self.shuffle_size / self.get_num_reducers()
        return long(math.floor((single_reduce_size/self.reduce_speed)*1000.))

    def __str__(self):
        return '{} {} {} {} {} {} {}'.format(self.jid
                                            ,self.arrival_time
                                            ,self.get_num_mappers()
                                            ,self.get_num_reducers()
                                            ,self.input_size
                                            ,self.shuffle_size
                                            ,self.output_size)

def parse_SWIM_job(raw_job,num_reduce_slots
                          ,block_size=64.*1024*1024
                          ,data_per_reducer=sys.maxint
                          ,cluster_size_ratio=1):
    pieces = raw_job.split()
    if len(pieces) != 6:
        sys.stderr.write("line {} doesn't rappresent a job{}".format(raw_job
                                                                  ,os.linesep))
        return None
    else:
        return SWIMJob(pieces[0]
                      ,long(pieces[1])
                      ,long(math.ceil(long(pieces[3])*cluster_size_ratio
                                                     /block_size))
                      ,long(pieces[4])*cluster_size_ratio
                      ,long(pieces[5])*cluster_size_ratio
                      ,num_reduce_slots
                      ,block_size=block_size
                      ,data_per_reducer=data_per_reducer)

def parse_SWIM_jobs(raw_jobs,num_reduce_slots
                            ,data_per_reducer=sys.maxint
                            ,cluster_size_ratio=1):
    res = []
    for raw_job in raw_jobs.split(os.linesep):
        job = parse_SWIM_job(raw_job,num_reduce_slots
                                    ,data_per_reducer=data_per_reducer
                                    ,cluster_size_ratio=cluster_size_ratio)
        if job:
            res.append(job)
    return res

def jobs_to_yaml(jobs):
    doc = ['jobs:']
    for job in jobs:
        job_str = []
        for attr in ['- jobid: {}'.format(job.jid)
                ,'  submitTime: {}'.format(job.arrival_time)
                ,'  numBlocks: {}'.format(job.num_blocks)
                ,'  blockSize: {}'.format(job.block_size)
                ,'  shuffleSize: {}'.format(job.shuffle_size)
                ,'  outputSize: {}'.format(job.output_size)
                ,'  totalMaps: {}'.format(job.get_num_mappers())
                ,'  mapDurationDistribution: ConstantRandomValue'
                ,'  mapDistributionParameters: {{value: {}}}'.format(
                                            job.get_mapper_duration())
                ,'  totalReduces: {}'.format(job.get_num_reducers())
                ,'  reduceDurationDistribution: ConstantRandomValue'
                ,'  reduceDistributionParameters: {{value: {}}}'.format(
                                            job.get_reducer_duration())]:
            job_str.append('  {}'.format(attr))
        doc.append(os.linesep.join(job_str))
    return os.linesep.join(doc)

if __name__=='__main__':
    p = mk_argparser()
    args = p.parse_args(sys.argv[1:])
    if args.original_cluster_size and args.cluster_size:
        cluster_size_ratio = (float(args.cluster_size) /
                              float(args.original_cluster_size))
    else:
        cluster_size_ratio = 1
    jobs = parse_SWIM_jobs(args.infile.read()
               ,long(args.cluster_size)*args.reduce_slots
               ,data_per_reducer=long(args.reducer_size) if args.reducer_size
                                                         else sys.maxint
               ,cluster_size_ratio=cluster_size_ratio)
    args.infile.close()
    filtered_jobs = []
    if args.min_maps:
        min_maps = long(args.min_maps)
    if args.min_reduces:
        min_reduces = long(args.min_reduces)
    for job in jobs:
        if args.min_maps and job.get_num_mappers() < min_maps:
            continue
        if args.min_reduces and job.get_num_reducers() < min_reduces:
            continue
        filtered_jobs.append(job)
    print(jobs_to_yaml(filtered_jobs))
