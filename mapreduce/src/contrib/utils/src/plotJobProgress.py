#!/usr/bin/env python
import argparse
import matplotlib
matplotlib.use('wx')
import os
import sys

from lib.job import *
from lib.plotutils import plot_two_sync_datasets,LinePlotter
from lib.strutils import should_process
from lib.mapreduce_base_script import mr_main 


def plot(d,multiplot):
    """ take a dictionary in the form jobid -> [(time,progress)] and plot one
        line per key

        time: in milliseconds
        progress: in milliseconds
    """
    xss,yss,jobtypes = [],[],[]
    xss2,yss2,jobtypes2 = ([],[],[]) if multiplot else (None,None,None)

    raw_jobtypes = d.keys()
    raw_jobtypes.sort()

    for jobtype in raw_jobtypes:
        ps = d[jobtype]
        jobid,ttype = jobtype
        xs,ys = [],[]
        for p in ps:
            xs.append(p[0]/1000.)
            ys.append(p[1]/1000.)
        if multiplot and ttype == 'r':
            xss2.append(xs)
            yss2.append(ys)
            jobtypes2.append(jobtype)
        else:
            xss.append(xs)
            yss.append(ys)
            jobtypes.append(jobtype)

    plotter = LinePlotter(xss,yss,jobtypes,'time (s)','job size (s)'
                         ,xss2=xss2,yss2=yss2,jobtypes2=jobtypes2)
    plot_two_sync_datasets(plotter,multiplot=multiplot)
    

def parse(content,jobids,task_type):
    res = {}
    for line in content.split(os.linesep):
        tokens = line.split()
        if len(tokens) != 3:
            print("WARN: line '{0}' is not splittable in three values"
                    .format(line))
            continue;
        [time,jobandtype,progr] = tokens
        pjt = jphasefromstr(jobandtype,sep=':')

        if not should_process(pjt,jobids,task_type):
            # print ('Ignoring {0} {1}'.format(pjt,jobids))
            continue

        try:
            pair = (long(time),long(progr))
            if pjt in res:
                res[pjt].append( pair )
            else:
                res[pjt] = [ pair ]
        except ValueError as e:
            print("WARN: line '{0}' contains invalid longs: {1}"
                        .format(line,e))
            continue
    return res

def plot_parse(content,jobids,task_type,multiplot):
    plot(parse(content,jobids,task_type),multiplot)

if __name__=='__main__':
    mr_main(sys.argv,plot_parse)
