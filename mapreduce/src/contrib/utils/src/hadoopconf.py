import argparse
import sys
import os
import xml.dom.minidom as xml

schedulers_shortcuts = { 
      'FIFO': 'org.apache.hadoop.mapred.JobQueueTaskScheduler'
    , 'HFS' : 'org.apache.hadoop.mapred.FairScheduler'
    , 'FAIR': 'org.apache.hadoop.mapred.FairScheduler'
    , 'HFSP': 'org.apache.hadoop.mapred.HFSPScheduler'}

shortcuts = {
    'taskscheduler':('mapreduce.jobtracker.taskscheduler',schedulers_shortcuts)
    }

def key_shortcut(key):
    return shortcuts[key][0] if key in shortcuts else None

def mkParser():
    p = argparse.ArgumentParser()
    p.add_argument('-f','--file')
    g = p.add_mutually_exclusive_group(required=True)
    g.add_argument('-s','--set', help="set key to value if exists. Form:" +
            " key=value", nargs="+")
    g.add_argument('-g','--get',help="keys to read",nargs='*')
    g.add_argument('-p','--print-shortcuts',help='print all the available' +
            ' shortcuts',action='store_true')
    return p

def main():
    p = mkParser()
    if len(sys.argv) < 2:
        p.print_usage()
        sys.exit(0)
    args = p.parse_args(sys.argv[1:])

    if args.set:
        toSet = dict()
        for raw_keyval in args.set:
            splitted = raw_keyval.split('=')
            if len(splitted) == 2:
                (key,value) = handle_shortcuts(splitted[0],splitted[1])
                toSet[key] = value
            else:
                sys.stderr.write('ERROR: keyval must be in the form key=val,'+
                            ' found ' + raw_keyval + os.linesep)
        properties_set(args.file,toSet)
    elif not args.get is None:
        props = properties_get(args.file,set(args.get))
        if props:
            for (key,values) in props.items():
                print('{0} = {1}'.format(key,' '.join(values)))
    elif args.print_shortcuts:
        print_shortcuts()

def print_shortcuts():
    to_print = []
    for (key,(full_key,values)) in shortcuts.items():
        to_print.extend( [key,'=',full_key])
        for (value,full_value) in values.items():
            to_print.extend([os.linesep,'\t',value,'=',full_value])
        to_print.append(os.linesep)
    print(' '.join(to_print[:-1]))

def handle_shortcuts(key,value):
    if key in shortcuts:
        (full_key,value_shortcuts) = shortcuts[key]
        if value in value_shortcuts:
            return (full_key,value_shortcuts[value])
        else:
            return (full_key,value)
    else:
        return (key,value)

def properties_set(conf_file,to_set):
    conf = read_conf(conf_file)
    properties = conf.getElementsByTagName('property')
    rewrite = False
    for p in properties:
        name = p.getElementsByTagName('name')[0].childNodes.item(0).data
        valNode = p.getElementsByTagName('value')[0]
        if name in to_set:
            valNode.childNodes.item(0).data = to_set[name]
            print('set {0} to {1}'.format(name,to_set[name]))
            rewrite = True

    if rewrite:
        write_conf(conf_file,conf)

def properties_get(conf_file,to_get):
    conf = read_conf(conf_file)
    properties = conf.getElementsByTagName('property')
    toShow = {}
    for p in properties:
        name = p.getElementsByTagName('name')[0].childNodes.item(0).data
        valNode = p.getElementsByTagName('value')[0]
        if not to_get or name in to_get:
            value = valNode.childNodes.item(0).data
            if name in toShow:
                toShow[name].append(value)
            else:
                toShow[name] = [value]

    return toShow

def property_set(conf_file,key,value):
    return properties_set(conf_file,dict([(key,value)]))

def property_get(conf_file,key):
    res = properties_get(conf_file,set([key]))
    if key in res:
        return properties_get(conf_file,set([key]))[key][0]
    else:
        return None

def read_conf(conf_file):
    if conf_file:
        with open(conf_file,'r') as f:
            content = f.read()
    else:
        content = sys.stdin.read()
    return xml.parseString(content)

def write_conf(conf_file,conf):
    if conf_file:
        with open(conf_file,'w') as f:
            conf.writexml(f)
    else:
        doc.writexml(sys.stdout)

if __name__=='__main__':
    main()
