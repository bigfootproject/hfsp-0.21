import argparse
import os
import os.path as path
import sys
from tempfile import mkstemp

from lib.job import parseJobSummaryLine

gnuplot_bin = '/usr/bin/gnuplot'

def mk_argparser():
    p = argparse.ArgumentParser()
    g = p.add_mutually_exclusive_group(required=True)
    g.add_argument('-m','--map-time',action='store_true')
    g.add_argument('-r','--reduce-time',action='store_true')
    g.add_argument('-s','--sojourn-time',action='store_true')
    p.add_argument('hfsp_file',type=argparse.FileType('rt'))
    p.add_argument('fair_file',type=argparse.FileType('rt'))
    p.add_argument('output',type=argparse.FileType('w'))
    return p

def plot_CDF(st_num,label):
    fd,temp_file = mkstemp()
    keys = sorted(st_num.keys())
    num_values = float(sum(st_num.values()))
    c = 0
    #xs,ys = [],[]
    with open(temp_file,'w') as h:
        for key in keys:
            c += st_num[key]
            #ys.append(c/num_values)
            #xs.append(key)
            h.write('{} {}{}'.format(c/num_values,key,os.linesep))
    return '"{}" using 2:1 w l t "{}"'.format(temp_file,label)

def parseFile(f,m):
    content = f.read()
    sojourn_times = {}
    for line in content.split(os.linesep):
        res = parseJobSummaryLine(line)
        if res:
            st = m(res)/60.
            #st = res.betweenMapReduceTime()
            if st in sojourn_times:
                sojourn_times[st] = sojourn_times[st] +1
            else:
                sojourn_times[st] = 1
    return sojourn_times

def main(gnuplot):
    p = mk_argparser()
    args = p.parse_args(sys.argv[1:])
    print('using {} as HFSP file{}      {} as FAIR file'
            .format(args.hfsp_file.name,os.linesep,args.fair_file.name))
    if args.sojourn_time:
        method = lambda j:j.sojournTime()
        xlabel = 'Sojourn Time [min]'
    elif args.map_time:
        method = lambda j:j.mapTime()
        xlabel = 'Map Time [min]'
    elif args.reduce_time:
        method = lambda j:j.reduceTime()
        xlabel = 'Reduce Time [min]'
    hfsp_sojourn_times = parseFile(args.hfsp_file,method)
    args.hfsp_file.close()
    fair_sojourn_times = parseFile(args.fair_file,method)
    args.fair_file.close()
    script_lines = []
    script_lines.append('set size 0.5,0.5')
    script_lines.append('set term postscript eps enhanced')
    script_lines.append('set key inside right bottom vertical Right '
                      + 'noreverse enhanced autotitles ')
    script_lines.append('set xlabel "{}"'.format(xlabel))
    #script_lines.append('set ylabel "ECDF"')
    script_lines.append('set yrange [0:1]')
    script_lines.append('set mytics 2')
    script_lines.append('set mxtics 2')
    #script_lines.append('set ytics (0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1)')
    script_lines.append('set output "{}"'.format(sys.argv[3]))
    hfsp_cdf = plot_CDF(hfsp_sojourn_times,'HFSP')
    fair_cdf = plot_CDF(fair_sojourn_times,'FAIR')

    if args.sojourn_time:
        script_lines.append('set arrow from 60,0 to 60,1 nohead lt 0')
        script_lines.append('plot {},{},0.5 notitle lt 0'
                .format(hfsp_cdf,fair_cdf))
    if args.map_time:
        script_lines.append('plot {},{},0.5 notitle lt 0'
                .format(hfsp_cdf,fair_cdf))
    if args.reduce_time:
        script_lines.append('plot {},{},0.5 notitle lt 0'
                .format(hfsp_cdf,fair_cdf))

    script = os.linesep.join(script_lines)
    #print(script)
    gnuplot.write(script)

if __name__=='__main__':
    if not path.exists(gnuplot_bin):
        sys.exit('Unable to locate gnuplot at {}'.format(gnuplot_bin))
    from subprocess import PIPE,Popen
    gnuplot = Popen([gnuplot_bin,'-p'],stdin=PIPE).stdin
    main(gnuplot)
