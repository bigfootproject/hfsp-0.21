#!/usr/bin/env python3

import argparse
from collections import Iterable
import os
import os.path as path
import sys

from lib.job import *
from lib.statistics import *
from lib.strutils import double_iter_to_str

from hadoopconf import key_shortcut, property_get

taskscheduler_key = key_shortcut('taskscheduler')

# map from column name to column index
name_to_ix = dict([(n,i) for (i,n) in enumerate(['nm','nr','m','d','r','s']
                                               ,start=1)])

def ix_sort(ix):
    return lambda a,b: cmp(a[ix],b[ix])

def get_mapred_site(basedir):
    h_mapred_site = path.join(basedir,'mumak-conf','mapred-site.xml')
    m_mapred_site = path.join(basedir,'hadoop-conf','mapred-site.xml')
    return h_mapred_site if path.exists(h_mapred_site) else m_mapred_site

def get_log_file(basedir):
    return path.join(basedir,'logs','mumak-jobs-summary.log')

def jobs_to_dict(jobs):
    return dict([(j.job_id,j) for j in jobs])

def parse_logs(jobsummary_log_file):
    c = ""
    with open(jobsummary_log_file,'rt') as f:
        c = f.read()
    lines = c.split(os.linesep)
    jobsummaries = []
    for line in lines:
        jobsummary = parseJobSummaryLine(line)
        if jobsummary:
            jobsummaries.append(jobsummary)
    return jobsummaries

def format_result(name,len_longest_name,num,len_longest_num):
    return ("{:<" + str(len_longest_name) + "} {:>" + str(len_longest_num)
            + "}").format(name + ":",num)

def format_dict(d,padding=" "):
    max_key_len = max([len(str(k)) for k in d.keys()])
    max_val_len = max([len(str(v)) for v in d.values()])
    res = []
    for key,val in d.items():
        try:
            i = iter(val)
            head = i.next()
            tail = []
            try:
                while True:
                    tail.append(i.next())
            except StopIteration:pass
            res.append(padding + format_result(key,max_key_len,head,max_val_len)
                     + '\t' + ' '.join(map(str,tail)))
        except TypeError:
            res.append(padding + format_result(key,max_key_len,val,max_val_len))
    return os.linesep.join(res)

def mk_stats(jobs_left,jobs_right,f):
    values = []
    min_jobids = None
    max_jobids = None
    for job_id in jobs_left:
        job_left,job_right = jobs_left[job_id],jobs_right[job_id]
        sojourn_time = f(job_left) - f(job_right)
        values.append(sojourn_time)
        # min
        if not min_jobids or min_jobids[0] > sojourn_time:
            min_jobids = (sojourn_time,[job_id])
        elif min_jobids[0] == sojourn_time:
            min_jobids[1].append(job_id)
        # max
        if not max_jobids or max_jobids[0] < sojourn_time:
            max_jobids = (sojourn_time,[job_id])
        elif max_jobids[0] == sojourn_time:
            max_jobids[1].append(job_id)

    s = stats(values)
    r = dict()
    r['avg'] = s[0]
    r['median'] = s[1]
    r['std_dev'] = s[2]
    r['confidence'] = s[5]
    r['min'] = min_jobids
    r['max'] = max_jobids
    return r

def main():
    parser = mkArgParser()
    if len(sys.argv) < 2:
        parser.print_help()
        sys.exit(0)
    args = parser.parse_args(sys.argv[1:])

    if args.sort_by and not args.jobs:
        print("WARN: sort option must be used with -j, I'm adding it")
        args.jobs=True

    for d in args.dirs:
        if not path.isdir(d):
            sys.exit("dir '{0}' doesn't exist or is not a directory".format(d))

    dir_left,dir_right = args.dirs[0],args.dirs[1]
    mapred_site_left = get_mapred_site(dir_left)
    if not path.isfile(mapred_site_left):
        sys.exit("left configuration file '{0}' doesn't exist"
               + " or is not a file".format(mapred_site_left))

    mapred_site_right = get_mapred_site(dir_right)
    if not path.isfile(mapred_site_right):
        sys.exit("right configuration file '{0}' doesn't exist"
               + " or is not a file".format(mapred_site_right))

    if not args.maps and not args.reduces:
        args.alljob = True

    log_file_left = get_log_file(dir_left)
    if not path.isfile(log_file_left):
        sys.exit("left log file '{0}' doesn't exist"
               + " or is not a file".format(log_file_left))

    log_file_right = get_log_file(dir_right)
    if not path.isfile(log_file_right):
        sys.exit("right log file '{0}' doesn't exist"
               + " or is not a file".format(log_file_right))

    jobs_left = jobs_to_dict(parse_logs(log_file_left))
    jobs_right = jobs_to_dict(parse_logs(log_file_right))

    if args.verbose:
        left_scheduler = property_get(mapred_site_left,taskscheduler_key)
        right_scheduler = property_get(mapred_site_right,taskscheduler_key)

        print("Left scheduler: {}{}Right scheduler: {}".format(left_scheduler
                                                             ,os.linesep
                                                             ,right_scheduler))
        if left_scheduler == "org.apache.hadoop.mapred.HFSPScheduler":
            print("left - right => POSITIVE is BAD")
        elif right_scheduler == "org.apache.hadoop.mapred.HFSPScheduler":
            print("left - right => NEGATIVE is BAD")
        print("-"*10)
    
    keys_left = set(jobs_left.keys())
    keys_right = set(jobs_right.keys())
    if keys_left != keys_right:
        keys_only_in_left = keys_left.difference(keys_right)
        keys_only_in_right = keys_right.difference(keys_left)
        sys.exit("The two job summary files aren't from the same workload"
               + os.linesep
               + "num left jobs: {},".format(len(jobs_left.keys()))
               + "\tnum right jobs: {})".format(len(jobs_right.keys()))
               + os.linesep + "jobs only in left: {}".format(keys_only_in_left)
               + os.linesep + "right jobs: {}".format(keys_only_in_right))

    if args.jobs:
        keys = jobs_left.keys()
        keys.sort()
        alljobs_acc = []
        for jobid in keys:
            job_left,job_right = jobs_left[jobid],jobs_right[jobid]
            acc = [jobid,job_left.num_maps,job_left.num_reduces]
            for f in [lambda j:j.mapTime()
                     ,lambda j:j.betweenMapReduceTime()
                     ,lambda j:j.reduceTime()
                     ,lambda j:j.sojournTime()]:
                acc.append(f(job_left)-f(job_right))
            acc.append((job_left.num_susp,job_right.num_susp))
            acc.append((job_left.num_resumed,job_right.num_resumed))
            alljobs_acc.append(acc)

        res = [['Job ID','#M','#R','Map Time','Middle Time','Reduce Time'
                       ,'Sojourn Time','#Susp l r','#Res l r']]
        if args.sort_by:
            comparer = ix_sort(name_to_ix[args.sort_by])
            alljobs_acc = map(lambda l:map(format_val,l)
                                            ,sorted(alljobs_acc
                                                    ,cmp=comparer))
        res.extend(alljobs_acc)
        print(double_iter_to_str(res) + os.linesep)

    if args.aggregates:
        for name,f in [ ('Map times:',lambda j:j.mapTime())
                      , ('Between map and reduce times:'
                            ,lambda j:j.betweenMapReduceTime())
                      , ('Reduce times:',lambda j:j.reduceTime())
                      , ('Sojourn times:',lambda j:j.sojournTime())
                    # ,('Exec times:',lambda j:j.execTime()) <- mumak bug!
                       ]:
            stats_res = mk_stats(jobs_left,jobs_right,f)
            to_format = []
            for key,val in stats_res.items():
                if isinstance(val,Iterable):
                    to_format.append([key] + list(map(format_val,val)))
                else:
                    to_format.append([key,format_val(val)])
            print(name)
            print(double_iter_to_str(to_format,padding=' '*3))


def format_val(val):
    """ Format the val such that can be easily read """
    if isinstance(val,str):
        return val
    elif isinstance(val,float):
        return '{:.1f}'.format(val)
    elif isinstance(val,Iterable):
        return ' '.join([format_val(i) for i in val])
    else:
        return str(val)


def mkArgParser():
    p = argparse.ArgumentParser(description='manage job summaries')
    p.add_argument('-a','--aggregates',action='store_true')
    p.add_argument('-j','--jobs',action='store_true')
    p.add_argument('-v','--verbose', action='store_true', help="Increase verbosity level")
    p.add_argument('-s','--sort-by',choices=name_to_ix.keys()
                  ,help='sort jobs by nm=numMaps nr=numReduces m=mapTime'
                       +' d=middleTime r=reduceTime s=sojournTime')
    group = p.add_mutually_exclusive_group(required=False)
    group.add_argument('--maps', action='store_true', help="Diff for map phase only")
    group.add_argument('--reduces', action='store_true', help="Diff for reduce phase only")
    group.add_argument('--alljob', action='store_true', help="Diff for all the job - DEFAULT")
    p.add_argument('dirs',nargs=2,metavar="<directory1> <directory2>")
    return p

if __name__=='__main__':
    main()
