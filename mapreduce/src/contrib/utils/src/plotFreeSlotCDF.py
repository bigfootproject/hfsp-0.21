import argparse
import os
import sys
import re
import matplotlib as MPL
MPL.use('wx')
import matplotlib.pyplot as PLT

avail_slots = re.compile('([0-9]+).*(MAP|REDUCE).*'
                       + 'tt.availableSlots: ([0-9]+) -> ([0-9]+)')

def mk_parser():
    p = argparse.ArgumentParser()
    p.add_argument('-t','--task-type',choices=['m','r']
                       ,required=False,default=None)
    p.add_argument('infile',metavar='input-file',type=argparse.FileType('rt'))
    return p

def main():
    p = mk_parser()
    if len(sys.argv) < 2:
        p.print_help()
        sys.exit()
    args = p.parse_args(sys.argv[1:])
    map_slots,reduce_slots = [],[]
    last=0
    for line in args.infile.read().split(os.linesep):
        matched = avail_slots.match(line)
        if matched and matched.group(3) != '0':
            time_avail_avail = (long(matched.group(1))
                               ,long(matched.group(3))
                               ,long(matched.group(4)))
            assert last <= time_avail_avail[0],'line {0}'.format(line)
            last = time_avail_avail[0]
            if should_plot_map(args.task_type) and matched.group(2) == 'MAP':
                map_slots.append(time_avail_avail)
            elif should_plot_reduce(args.task_type):
                reduce_slots.append(time_avail_avail)
    args.infile.close()
    if should_plot_map(args.task_type):
        plot_cdf(map_slots,'Map')
    if should_plot_reduce(args.task_type):
        plot_cdf(reduce_slots,'Reduce')
    PLT.xlabel('time (s)')
    PLT.ylabel('CDF')
    PLT.legend()
    PLT.grid()
    PLT.show()

def should_plot_map(task_type):
    return not task_type or task_type == 'm'

def should_plot_reduce(task_type):
    return not task_type or task_type == 'r'

def plot_cdf(ls,label=None):
    d = {}
    for fst,snd in zip(ls,ls[1:]):
        time1 = fst[0]
        time2 = snd[0]
        interval = time2 - time1
        assert interval >= 0,'{0} {1}'.format(time1,time2)
        num = snd[1]
        d[interval] = num
    intervals = d.keys()
    intervals.sort()
    acc = 0
    xs,ys = [],[]
    for interval in intervals:
        xs.append(interval/1000.)
        acc += interval * d[interval]
        ys.append(acc)
    PLT.plot(xs,[float(y)/acc for y in ys],'o-',label=label)

if __name__=="__main__":
    main()
