#!/usr/bin/env python3

import argparse
from glob import glob
import os
import os.path as path
import subprocess as sub
import shutil as sh
import sys

from hadoopconf import property_set,handle_shortcuts

import xmlrpc.client
rcpServer = xmlrpc.client.ServerProxy('http://eurecom:Aingae1a@voidnull.eu:80')


def mkArgParser():
    p = argparse.ArgumentParser(description='run tests and collect the results')
    p.add_argument('-D','--set-property',metavar='property=value',nargs='+'
                       ,help='java properties that will be passed to the JVM'
                       ,default=[],required=False)
    p.add_argument('-s','--scheduler',metavar='scheduler',nargs='?',
                                      help='set the scheduler')
    p.add_argument('workloads',nargs='+',help='workload to use')
    p.add_argument('-M','--machines',metavar='num',
                              help='machines number to test')
    p.add_argument('-m','--maps',metavar='num',
                          help='map slot number for machine')
    p.add_argument('-r','--reduces',metavar='num',
                          help='reduce slot number for machine')
    p.add_argument('-f','--force',action='store_true',
                          help='remove old logs')
    p.add_argument('-o','--output_dir',help='output directory')
    p.add_argument('-z','--zip',action='store_true',
                    help='should zip the results')
    p.add_argument('-d','--dummy',action='store_true',
                    help='print actions without doing them')
    p.add_argument('-t','--tag',help='tag to prepends',default=None)
    p.add_argument('--enableRpc', action='store_true',
                    help='Enable the use of a remote RPC coordinator, to divide the experiments across multiple workers')
    return p

def main():
    scriptdir = path.dirname(path.abspath(sys.argv[0]))
    defoutdir = path.join(scriptdir,'mumakresults')
    mumakDir = path.join(dirname(scriptdir,2),'mumak')
    mumakConfDir = path.join(mumakDir,'conf')
    mapredSite = path.join(mumakConfDir,'mapred-site.xml')
    mumakNext = path.join(mumakDir,'bin','mumakNext.sh')
    if not path.exists(mumakNext):
        sys.exit("cannot find mumakNext.sh (searched: {0})".format(mumakNext))
    logDir = path.join(dirname(scriptdir,4),'logs')

    parser = mkArgParser()
    if len(sys.argv) < 1:
        parser.print_help()
        sys.exit()
    args = parser.parse_args(sys.argv[1:])

    outdir = path.abspath(args.output_dir) if args.output_dir else defoutdir
    if not path.exists(outdir) and not args.dummy:
        os.mkdir(outdir)

    for workload in args.workloads:
        if not path.exists(workload):
            sys.exit("Workload {0} doesn't exist".format(workload))

    machines = int(args.machines) if args.machines else -1
    maps = int(args.maps) if args.maps else -1
    reduces = int(args.reduces) if args.reduces else -1
    scheduler = args.scheduler

    outdirpieces = []
    if args.tag:
        outdirpieces.append(args.tag)
    if machines != -1:
        outdirpieces.append('M{0}'.format(machines))
    if maps != -1:
        outdirpieces.append('m{0}'.format(maps))
    if reduces != -1:
        outdirpieces.append('r{0}'.format(reduces))
    if args.scheduler:
        outdirpieces.append(args.scheduler)

    collectdir = path.join(outdir,'_'.join(outdirpieces))
    prepareOutputPath(collectdir,args.force)

    stdoutFile = path.join(collectdir,'stdout')
    stderrFile = path.join(collectdir,'stderr')

    if not args.enableRpc or rcpServer.bookExperiment(outdirpieces, args.dummy):
        print('starting benchmark with {0}'.format(outdirpieces))

        if args.scheduler and not args.dummy:
            key,value = handle_shortcuts('taskscheduler',scheduler)
            property_set(mapredSite,key,value)
            
        if args.force:
            cleanLogs(logDir)

        execMumak(mumakNext,args.workloads,machines,maps,reduces,args.dummy
                 ,stdoutFile=stdoutFile,stderrFile=stderrFile
                 ,jvm_properties=args.set_property)

        if not args.dummy:
            print('{0}collecting data:'.format(os.linesep))
            collect('../../../../../benchmark-logs',collectdir,args.zip)
            print('collected data in ' + collectdir 
                  + (' and zipped' if args.zip else ''))

        if args.enableRpc:
            rcpServer.experimentDone(outdirpieces, args.dummy)
    else:
        print('Skipping benchmark with {0}'.format(outdirpieces))


def dirname(name,level):
    for i in range(level):
        name = path.dirname(name)
    return name

def cleanLogs(logDir,dummy=False):
    if path.exists(logDir):
        if not dummy:
            sh.rmtree(logDir)
        print('removed log directory {0}'.format(logDir))

def execMumak(mumakpath,workloads,M=-1,m=-1,r=-1,dummy=False
             ,stdoutFile=None,stderrFile=None,jvm_properties=[]):
    if workloads == None or len(workloads) == 0:
        sys.exit('need at least one workload')
    execPieces = ['/bin/bash',mumakpath]
    if jvm_properties:
        for jvm_property in jvm_properties:
            execPieces.extend(['-D', jvm_property])
    execPieces.extend(['-p','STATIC'])
    if M != -1:
        execPieces.extend(['-M',str(M)])
    if m != -1:
        execPieces.extend(['-m',str(m)])
    if r != -1:
        execPieces.extend(['-r',str(r)])
    execPieces.extend(['-j',','.join(workloads)])
    print("out and err files set to {0} and {1}".format(stdoutFile,stderrFile))
    print("exec: '{0}'".format(' '.join(execPieces)))
    if not dummy:
        out = open(stdoutFile,'w') if stdoutFile else sys.stdout
        err = open(stderrFile,'w') if stderrFile else sys.stderr
        exitCode = sub.Popen(execPieces,stdout=out,stderr=err).wait()
        if stdoutFile:
            out.close()
        if stderrFile:
            err.close()
    return None

def collect(scriptdir,outdir,shouldZip):
    outputpath = outdir
    if not path.isabs(outputpath):
        outputpath = join(scriptdir,outputpath)
    
    rootdir = path.dirname(scriptdir)
    hadoopdir = path.join(rootdir,"hadoop-mapreduce-0.21")
    
    hadoopconfdir = path.join(hadoopdir,"conf")
    logfiles = glob(path.join(hadoopdir,"logs","*.log"))
    mumakconfdir = path.join(hadoopdir,"src","contrib","mumak","conf")
    
    tmphadoopconfdir = path.join(outputpath,"hadoop-conf")
    tmplogdir = path.join(outputpath,"logs")
    tmpmumakconfdir = path.join(outputpath,"mumak-conf")
    
    print("Copying {0} to {1}".format(hadoopconfdir,tmphadoopconfdir))
    sh.copytree(hadoopconfdir,tmphadoopconfdir)
    os.mkdir(tmplogdir)
    for logfile in logfiles:
        print("Copying {0} into {1}".format(logfile,tmplogdir))
        sh.copy2(logfile,tmplogdir)
    print("Copying {0} to {1}".format(mumakconfdir,tmpmumakconfdir))
    sh.copytree(mumakconfdir,tmpmumakconfdir)

    print("Copying files to {0} done".format(outputpath))
    
    if shouldZip:
        files = glob(path.join(outputpath,"*"))
        zippedfile = outputpath + '.7z'
        print("Zipping " + outputpath + " to " + zippedfile)
        zip(outputpath, zippedfile, files, False)

def prepareOutputPath(outputpath,force):
    if path.exists(outputpath):
        if force:
            sh.rmtree(outputpath)
        else:
            sys.exit('Output directory ' + outputpath + ' exists, add -f to'
                    + ' remove it')
    
    os.mkdir(outputpath)

def zip(outputpath,outputfile,files, force=False):
    if path.exists(outputfile):
        if force:
            remove(outputfile)
        else:
            sys.exit('Output file ' + outputfile + 'exists, add -f to'
                    + ' remove it')

    chdir(outputpath)
    
    out,err = sub.Popen(['7z','a','-r',outputfile] + files).communicate()

if __name__ == '__main__':
    main()
