#!/usb/bin/env python
import argparse
from collections import deque
import matplotlib
matplotlib.use('wx')
import os
import sys

from lib.job import *
from lib.plotutils import plot_two_sync_datasets,RectPlotter
from lib.rect import line_to_rects
from lib.strutils import should_process
from lib.mapreduce_base_script import mr_main 


class Counter(object):
    def __init__(self):
        self.d = {}

    def reset_last(self,x):
        for (xs,ys) in self.d.values():
            if xs and xs[-1] == x:
                xs.pop()
                ys.pop()

    def add_or_incr(self,key,x):
        if key in self.d:
            xs,ys = self.d[key]
            if xs and xs[-1] == x:
                ys[-1] = ys[-1] + 1
                #print key,x,ys[-1]
            else:
                xs.append(x)
                ys.append(1)
        else:
            self.d[key] = (deque([x]),deque([1]))


def plot(counter,multiplot):
    d = counter.d
    raw_jobtypes = d.keys()
    raw_jobtypes.sort()

    lines1,jobtypes = [],[]
    lines2,jobtypes2 = ([],[]) if multiplot else (None,None)

    for jobtype in raw_jobtypes:
        jobid,ttype = jobtype
        if multiplot and ttype == 'r':
            lines2.append( d[jobtype] )
            jobtypes2.append(jobtype)
        else:
            lines1.append( d[jobtype] )
            jobtypes.append(jobtype)

    rectplotter = RectPlotter(lines1,jobtypes,'time (s)','slots'
                             ,lines2=lines2,jobtypes2=jobtypes2)
    plot_two_sync_datasets(rectplotter,multiplot=multiplot)
    

def parse(content,jobids,task_type):
    counter = Counter()

    processed = set()

    for line in content.split(os.linesep):
        tokens = line.split()
        if len(tokens) != 3:
            print("WARN: line '{0}' is not splittable in three values"
                    .format(line))
            continue;
        [time,raw_task,progr] = tokens

        if progr == '0':
            print("WARN: ignoring line '{0}' because progress is 0".format(line))
            continue

        if not time.isdigit():
            print("WARN: ignoring line '{0}' beacause the time {1} is not a long"
                    .format(line,time))
            continue

        ltime = long(time)/1000.0
        task = taskfromstr(raw_task,sep='_')

        if not task:
            print("WARN: word {0} doesn't represent a task".format(raw_task))
            continue

        jobphase = task[0:2]

        if not should_process(jobphase,jobids,task_type):
            # print ('Ignoring {0} {1}'.format(pjt,jobids))
            continue
        
        if (time,task) in processed:
            print("WARN: line '{0}' is a duplicate, skipping it".format(line))
            counter.reset_last(ltime)
            processed.clear()
            #continue

        processed.add( (time,task) )
        
        #print ('time jobid type taskid prog: {0} {1} {2} {3} {4}'
        #                        .format(time,task[0],task[1],task[2],progr))
        counter.add_or_incr(jobphase,ltime)
    return counter

def plot_parse(content,jobids,task_type,multiplot):
    parsed = parse(content,jobids,task_type)
    #fk = parsed.d.keys()[0]
    #print('{0}: {1}'.format(fk,parsed.d[fk][1]))
    plot(parsed,multiplot)

if __name__=='__main__':
    mr_main(sys.argv,plot_parse)
