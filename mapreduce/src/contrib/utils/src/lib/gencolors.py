from colorsys import hsv_to_rgb
from random import random

# use golden ratio
golden_ratio_conjugate = 0.618033988749895
h = 0.5 # random() # use random start value
def gencolor(s=0.5,v=0.95):
    """ generate random rgb color """
    global h
    h = (h + golden_ratio_conjugate) % 1
    return hsv_to_rgb(h, s, v)
