from collections import deque
from copy import deepcopy

from gencolors import gencolor


class Rectangle(object):
    """
         ________
        |        |
        |        h
        |____w___|
    (x,y)

    """
    def __init__(self,(x,y),width,height):
        self.xy = (x,y) # following matplotlib conventions
        self.width = width
        self.height = height

    def __repr__(self):
        return 'Rect({0},{1},{2})'.format(self.xy,self.width,self.height)

    def __eq__(self,o):
        return (isinstance(o,self.__class__)
                and self.xy == o.xy
                and self.width == o.width 
                and self.height == o.height)

    def set_x(self,x):
        self.xy = (x,self.xy[1])
        return self

    def set_y(self,y):
        self.xy = (self.xy[0],y)
        return self

    def mod_x(self,f):
        return self.set_x(f(self.xy[0]))

    def mod_y(self,f):
        return self.set_y(f(self.xy[1]))

    def get_centre(self):
        return (self.xy[0] + self.width/2., self.xy[1] + self.height/2.)

    def get_topright(self):
        return (self.xy[0] + self.width, self.xy[1] + self.height)

    def get_bottomright(self):
        return (self.xy[0] + self.width, self.xy[1])

    def get_topleft(self):
        return (self.xy[0],self.xy[1] + self.height)

    def lift_on_top_of(self,o):
        """Move this on top of o"""
        return self.mod_y(lambda y: y + o.get_topright()[1])

    def intersect_x(self,where):
        """
            note: boundaries are not considered
        """
        return where > self.xy[0] and where < self.xy[0] + self.width

    def split_x(self,where):
        if self.intersect_x(where):
            return [Rectangle(self.xy,where-self.xy[0],self.height),
                    Rectangle((where,self.xy[1]),
                              self.width-(where-self.xy[0]),
                              self.height)]
        else:
            return [self]

def line_to_rects(line,label=None):
    """
        line should be ([x1,...,xn],[y1,...,yn])
    """
    assert type(line) == tuple,type(line)
    rects = deque()

    last_x,last_w,last_h = 0,0,0
    #print line
    for idx in range(len(line[0])):
        point_x,point_y = line[0][idx],line[1][idx]
        if last_h > 0:
            if last_h == point_y: # extend the rectangle
                last_w = point_x - last_x
            else: # add curr rectangle and start a new one
                if last_h > 0:
                    rects.append(Rectangle((last_x,0),point_x - last_x,last_h))
                last_x,last_w,last_h = point_x,0,point_y
        else:
            last_x,last_h = point_x,point_y
    if last_h > 0:
        #print last_w
        rects.append(Rectangle((last_x,0),last_w,last_h))

    return ListOfRects(rects,label=label,color=gencolor())

def stack_rects(lists_of_rects):
    """get a list of ListOfRects and stack them"""
    if not lists_of_rects:
        return []
    
    results = [deepcopy(lists_of_rects[0])] # final result
    last_res = deepcopy(lists_of_rects[0].rects) # last stacked list_of_rects
    for list_of_rects in deepcopy(lists_of_rects[1:]):
        new_last_res = deque()
        result = deque() # store the result for list_of_rects
        while not list_of_rects.is_empty():
            #print list_of_rects,last_res
            if not last_res:
                result.extend(list_of_rects.rects)
                new_last_res.extend(list_of_rects.rects)
                break

            curr_rect = list_of_rects.rects.popleft()
            while last_res:
                last_rect = last_res.popleft()
                last_x = last_rect.xy[0]
                last_r = last_rect.get_bottomright()[0]
                curr_x = curr_rect.xy[0]
                curr_r = curr_rect.get_bottomright()[0]

                if curr_x >= last_r:
                    new_last_res.append(last_rect)
                    if last_res:
                        continue # next last_rect
                    else:
                        result.append(curr_rect)
                        break # next curr_rect

                if last_x >= curr_r:
                    new_last_res.append(curr_rect)
                    last_res.appendleft(last_rect)
                    result.append(curr_rect)
                    break # next curr_rect

                same_left_x = (curr_x == last_x)
                same_right_x = (curr_r == last_r)

                # curr_rect is exactly above last_rect
                if same_left_x and same_right_x:
                    curr_rect.lift_on_top_of(last_rect)
                    result.append(curr_rect)
                    new_last_res.append(curr_rect)
                    break # next curr_rect and next last_rect

                curr_x_intersect = last_rect.intersect_x(curr_x)
                curr_r_intersect = last_rect.intersect_x(curr_r)

                if same_left_x:
                    if curr_r_intersect:
                        curr_rect.lift_on_top_of(last_rect)
                        result.append(curr_rect)
                        last_res.appendleft(last_rect)
                        new_last_res.append(curr_rect)
                        last_res.appendleft(last_rect.split_x(curr_r)[1])
                        break # next curr_rect
                    else:
                        fst,snd = curr_rect.split_x(last_r)
                        fst.lift_on_top_of(last_rect)
                        result.append(fst)
                        new_last_res.append(fst)
                        if last_res:
                            curr_rect = snd
                            continue # next last_rect
                        else:
                            result.append(snd)
                            new_last_res.append(snd)
                            break # next curr_rect
                
                if same_right_x:
                    if curr_x_intersect:
                        curr_rect.lift_on_top_of(last_rect)
                        result.append(curr_rect)
                        new_last_res.append(last_rect.split_x(curr_x)[0])
                        new_last_res.append(curr_rect)
                    else:
                        fst,snd = curr_rect.split_x(last_x)
                        snd.lift_on_top_of(last_rect)
                        result.append(fst)
                        result.append(snd)
                        new_last_res.append(fst)
                        new_last_res.append(snd)
                    break # next curr_rect, next last_rect
                        
                if curr_x_intersect and curr_r_intersect:
                    curr_rect.lift_on_top_of(last_rect)
                    result.append(curr_rect)
                    last_fst,last_snd = last_rect.split_x(curr_x)
                    last_snd,last_trd = last_snd.split_x(curr_r)
                    new_last_res.append(last_fst)
                    new_last_res.append(curr_rect)
                    last_res.appendleft(last_trd)
                    break # next curr_rect

                if curr_x_intersect:
                    last_fst,last_snd = last_rect.split_x(curr_x)
                    new_last_res.append(last_fst)
                    fst,snd = curr_rect.split_x(last_r)
                    fst.lift_on_top_of(last_rect)
                    result.append(fst)
                    new_last_res.append(fst)
                    if last_res:
                        curr_rect = snd
                        continue # next last_rect
                    else:
                        result.append(snd)
                        new_last_res.append(snd)
                        break # next curr_rect

                if curr_r_intersect:
                    fst,snd = curr_rect.split_x(last_x)
                    snd.lift_on_top_of(last_rect)
                    result.append(fst)
                    result.append(snd)
                    new_last_res.append(fst)
                    new_last_res.append(snd)
                    last_res.appendleft(last_rect)
                    break # next curr_rect

                last_x_intersect = curr_rect.intersect_x(last_x)
                last_r_intersect = curr_rect.intersect_x(last_r)

                if last_x_intersect and last_x_intersect:
                    fst,snd = curr_rect.split_x(last_x)
                    snd,trd = snd.split_x(last_r)
                    snd.lift_on_top_of(last_rect)
                    result.append(fst)
                    result.append(snd)
                    new_last_res.append(fst)
                    new_last_res.append(snd)
                    if last_res:
                        curr_rect = trd
                        continue # next last_rect
                    else:
                        result.append(trd)
                        new_last_res.append(trd)
                        break # next curr_rect

                raise Exception()
        

        results.append(ListOfRects(deepcopy(result)
                                  ,label=list_of_rects.label
                                  ,color=list_of_rects.color))
        if last_res: # copying the rest of the rectangles
            new_last_res.extend(last_res)
        last_res = new_last_res
    return results

class ListOfRects(object):
    """A list of rectangles with a color and a label"""

    def __init__(self,rects,label=None,color=None):
        self.rects = rects
        self.label = label
        self.color = color

    def __repr__(self):
        return 'ListOfRect({0},color={1},{2})'.format(self.label
                                                     ,self.color
                                                     ,list(self.rects))

    def __eq__(self,o):
        return (isinstance(o,self.__class__)
                and self.label == o.label
                and self.rects == o.rects)
    
    def is_empty(self):
        return not self.rects


if __name__ == '__main__':
    from plotutils import *
    rs0 = line_to_rects(([0,1,2,2,3,5,7,8,9,10],[2,2,2,5,5,5,0,0,4,4])
                         ,label='rs0')
    rs1 = line_to_rects(([0,1,2,2,3,5,7,8,9,10],[2,2,2,5,5,5,0,0,4,4])
                         ,label='rs1')
    srss = stack_rects([rs0,rs1]);
    plot_all(srss)
    plot.show()
