#!/usr/bin/env python
import argparse
import os
from lib.strutils import parse_job_defs


def mkparser():
    p = argparse.ArgumentParser()
    p.add_argument('-j','--jobs',default=[],help='jobs')
    p.add_argument('-t','--task-type'
                ,choices=['m','r','a'],default='a')
    p.add_argument('-m','--multiplot',action='store_true')
    p.add_argument('infile',metavar='input-file',type=argparse.FileType('rt'))
    return p
 

def mr_main(argv,parse_and_plot):
    p = mkparser()
    args = p.parse_args(argv[1:])

    content = args.infile.read()
    args.infile.close()
    jobids = parse_job_defs(args.jobs)
    task_type = args.task_type

    parse_and_plot(content,jobids,task_type,args.multiplot)
