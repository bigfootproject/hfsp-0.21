import matplotlib as MPL
import matplotlib.pyplot as PLT
from matplotlib.widgets import Button

from rect import line_to_rects,stack_rects

def plot_rect(rect,ax=None,label=None,**kwargs):
    mplrect = MPL.patches.Rectangle(rect.xy,rect.width,rect.height,**kwargs)
    ax2 = ax if ax else PLT.gca()
    ax2.add_patch(mplrect)
    if label:
        (x,y) = rect.get_centre()
        ax2.text(x,y,label,horizontalalignment='center'
                           ,verticalalignment='center')

def plot_listofrects(lor,ax=None):
    xmin,xmax,ymin,ymax = 0,0,0,0
    for rect in lor.rects:
        plot_rect(rect,label=lor.label,color=lor.color,ax=ax)
        if xmin > rect.xy[0]:
            xmin = rect.xy[0]
        if ymin > rect.xy[1]:
            ymin = rect.xy[1]
        x,y = rect.get_topright()
        if xmax < x:
            xmax = x
        if ymax < y:
            ymax = y
    return xmin,xmax,ymin,ymax

def plot_all(lists_of_rects,ax=None,xspan=1,yspan=0.25):
    """Plot a list of ListOfRects"""
    xmin,xmax,ymin,ymax = 0,0,0,0
    for lor in lists_of_rects:
        c_xmin,c_xmax,c_ymin,c_ymax = plot_listofrects(lor,ax)
        xmin = min(xmin,c_xmin-xspan)
        ymin = min(ymin,c_ymin-yspan)
        xmax = max(xmax,c_xmax+xspan)
        ymax = max(ymax,c_ymax+yspan)
    print xmin
    ax2 = PLT.gca() if ax is None else ax
    ax2.set_xlim(xmin,xmax)
    ax2.set_ylim(ymin,ymax)

class SyncSubplots(object):
    def __init__(self,getter,setters):
        self.getter = getter
        self.setters = setters

    def sync(self,event):
        xy = self.getter()
        for setter in self.setters:
            setter(xy)
        PLT.draw()

buttons = {} # FIXME: hack, without that buttons are not clickable
def add_button(x,c1,c2,label):
    button_axes = PLT.axes([x,0.02,0.2,0.075])
    sync_action = SyncSubplots(c1,[c2])
    global buttons
    buttons[x] = Button(button_axes,label=label)
    buttons[x].on_clicked(sync_action.sync)

def jobtype_to_label(jobtype):
    return '{0} {1}'.format(jobtype[0],jobtype[1])

class MultiPlotter(object):
    def __init__(self,xlabel,ylabel,ylabel2=None):
        self._xlabel = xlabel
        self._ylabel = ylabel
        self._ylabel2 = ylabel2

    def xlabel(self,num):
        return self._xlabel

    def ylabel(self,num):
        if num == 1:
            return self._ylabel
        elif num == 2:
            return self._ylabel2 if not self._ylabel2 is None else self._ylabel

    def plot(self,num,ax): pass

class LinePlotter(MultiPlotter):
    def __init__(self,xss,yss,jobtypes,xlabel,ylabel
                     ,xss2=None,yss2=None,jobtypes2=None,ylabel2=None):
        MultiPlotter.__init__(self,xlabel,ylabel,ylabel2)
        self._xss=xss
        self._yss=yss
        self._jobtypes=jobtypes
        self._xss2=xss2
        self._yss2=yss2
        self._jobtypes2=jobtypes2

    def plot(self,num,ax):
        if num == 1:
            for xs,ys,jobtype in zip(self._xss,self._yss,self._jobtypes):
                ax.plot(xs,ys,label=jobtype_to_label(jobtype))
        elif num == 2:
            for xs,ys,jobtype in zip(self._xss2,self._yss2,self._jobtypes2):
                ax.plot(xs,ys,label=jobtype_to_label(jobtype))

class RectPlotter(MultiPlotter):
    def __init__(self,lines1,jobtypes,xlabel,ylabel
                     ,lines2=None,jobtypes2=None,ylabel2=None):
        MultiPlotter.__init__(self,xlabel,ylabel,ylabel2)
        self.rects1 = RectPlotter.stack(lines1,jobtypes) 
        self.rects2 = RectPlotter.stack(lines2,jobtypes2) if lines2 else None

    @staticmethod
    def stack(lines,jobtypes):
        res = []
        for line,jobtype in zip(lines,jobtypes):
            res.append(line_to_rects(line,label=jobtype_to_label(jobtype)))
        return stack_rects( res )

    def plot(self,num,ax):
        if num == 1:
            print('Plotting in the first')
            plot_all(self.rects1,ax)
        elif num == 2:
            plot_all(self.rects2,ax)


def plot_two_sync_datasets(plotter,multiplot=False):
    """Use a plotter to draw two plots synchronized"""
    
    ax1 = PLT.subplot(2 if multiplot else 1,1,1)
    PLT.ylabel(plotter.ylabel(1))
    if multiplot:
        ax2 = PLT.subplot(2,1,2)
        PLT.ylabel(plotter.ylabel(2))

    plotter.plot(1,ax1)

    if multiplot:
        plotter.plot(2,ax2)

    ax1.legend()
    ax1.grid()
    if multiplot:
        ax2.legend()
        ax2.grid()

    PLT.xlabel(plotter.xlabel(1))

    if multiplot:
        PLT.subplots_adjust(bottom=0.2)
        
        add_button(0.1 ,ax1.get_xlim,ax2.set_xlim,'X sync bottom with top')
        add_button(0.31,ax2.get_xlim,ax1.set_xlim,'X sync top with bottom')
        add_button(0.52,ax2.get_ylim,ax1.set_ylim,'Y sync top with bottom')
        add_button(0.73,ax1.get_ylim,ax2.set_ylim,'Y sync bottom with top')

    PLT.show()
