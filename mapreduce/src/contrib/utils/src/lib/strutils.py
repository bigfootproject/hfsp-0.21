
def parse_job_def(rawjob):
    if not rawjob:
        return None
    
    if rawjob[-1] in ['a','m','r']:
        phase = rawjob[-1]
        jobid = rawjob[:-1]
        if not jobid.isdigit():
            return None
        return (int(jobid),phase)
    elif rawjob.isdigit():
        return (int(rawjob),None)
    else:
        return None

def parse_job_defs(rawjobs):
    """
        >>> parse_job_def('1')
        {'1':None}
        >>> parse_job_def('1m')
        {'1':'m'}
        >>> parse_job_def('1a')
        {'1':'a'}
        >>> parse_job_def('1-3r')
        {'1':'r','2':'r','3':'r'}
        >>> parse_job_def('5r,7a')
        {'5':'r','7':'a'}
    """
    if not rawjobs:
        return dict()
    result = dict()
    jobs = rawjobs.split(',')
    for job in jobs:
        if '-' in job:
            for (jobid,ttype) in parse_range(job):
                result[jobid] = ttype
        else:
            parsed = parse_job_def(job)
            if not parsed:
                print('Ignoring {0}'.format(job))
                continue
            result[parsed[0]] = parsed[1] if len(parsed) > 1 else None
    return result

def parse_range(job):
    begin,endtype = job.split('-')
    if not begin.isdigit():
        print('Ignoring {0}'.format(job))
        return []
    parsed = parse_job_def(endtype)
    if not parsed:
        print('Ignoring {0}'.format(job))
        return []
    (end,ttype) = parsed
    begintoend = range(int(begin),int(end)+1)
    return [(num,ttype) for num in begintoend]

def should_process(pjt,jobids,default_ttype='a'):
    """Return if pjt should be processed considering jobids and a def param
        >>> should_process(('1','m'),{},default_ttype='a')
        True
        >>> should_process(('1','m'),{},default_ttype='m')
        True
        >>> should_process(('1','m'),{},default_ttype='r')
        False
        >>> should_process(('1','m'),{'1':'a'},default_ttype='r')
        True
        >>> should_process(('1','m'),{'1':'m'},default_ttype='r')
        True
        >>> should_process(('1','m'),{'1':'r'},default_ttype='r')
        False
        >>> should_process(('1','m'),{'1':'r'},default_ttype='m')
        False
    """
    if pjt is None:
        return False
    jobid,ttype = pjt
    if jobids:
        if not jobid in jobids:
            return False
        should_ttype = jobids[jobid]
        return (should_ttype == 'a' or 
                (should_ttype == None 
                    and should_process_default(ttype,default_ttype))
                or should_ttype == ttype)
    else:
        return should_process_default(ttype,default_ttype)

def should_process_default(ttype,default_ttype):
    return default_ttype == 'a' or default_ttype == ttype

import itertools as IT
import os

def double_iter_to_str(ii,space=' ',padding='',aligns=None):
    """ transform a double iterator into a printable table """
    if not ii:
        return ''
    columns = max([len(i) for i in ii])
    if not aligns:
        aligns = list(IT.chain(['<'],IT.repeat('>',columns-1)))
    cells_len = [0 for c in range(columns)]
    for i in ii:
        for c in range(columns):
            if c >= len(i):
                break
            cells_len[c] = max(cells_len[c],len(str(i[c])))
    acc = []
    for i in ii:
        i_acc = []
        for c in range(columns):
            if c >= len(i):
                break
            e = i[c]
            align = aligns[c]
            cell_len = cells_len[c]
            i_acc.append(padding + ('{:' + align + str(cell_len) 
                                 + '}').format(e))
        acc.append(space.join(i_acc))
    return os.linesep.join(acc)
