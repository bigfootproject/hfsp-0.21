import re

jobtypes = set(['m','r'])

def typefromstr(s):
    if s in ['m','MAP']:
        return 'm'
    if s in ['r','REDUCE']:
        return 'r'
    return None

def jobfromstr(s):
    """Return the job id retrieved from the given string

    Accept jobs in the form <prefix>_<jobid> where <prefix>_ can be omitted

    >>> jobfromstr("test_0001")
    1
    >>> jobfromstr("0002")
    2
    """
    i = s.rfind("_")
    jid = s if i < 0 else s[(i+1):]
    return int(jid)

def jphasefromstr(s,sep='_'):
    """Return the tuple (jobid,jobtype) from the given string or None

    Accept job phase in the form <job><sep>[MAP|REDUCE] and return
    the tuple (jobid,jobtype) or None if the string format is not correct

    >>> jphasefromstr('job_fakejt_0001:MAP',sep=':')
    (1,'m')
    >>> jphasefromstr('error')
    None
    """
    i = s.rfind(sep)
    if i <= 0:
        return None
    t = typefromstr(s[i+1:])
    if t is None:
        return None
    return (jobfromstr(s[0:i]),t)

def taskfromstr(s,sep='_'):
    """Return the triple (jobid,jobtype,taskid) from the given string or None

    Accept task in the form <job><sep>[m|r]<sep><taskid> and return
    the triple (jobid,jobtype,taskid) or None if the string format is not
    correct

    >>> taskfromstr('task_fakejt_0000_r_000011')
    (0, 'r', 11)
    >>> taskfromstr('error')
    None
    """
    i = s.rfind('_')
    if i <= 0:
        return None
    (jphase,tid) = (jphasefromstr(s[0:i],sep=sep),s[i+1:])
    if jphase is None:
        return None
    return (int(jphase[0]),jphase[1],int(tid))

class JobSummary(object):
    def __init__(self, summary_creation
                     , job_id
                     , submit_time
                     , launch_time
                     , last_mapper_finish_time
                     , first_reducer_start_time
                     , finish_time
                     , num_maps
                     , num_slots_per_map
                     , num_reduces
                     , num_slots_per_reduce
                     , num_susp
                     , num_resumed
                ):
        assert submit_time <= launch_time <= last_mapper_finish_time and \
               first_reducer_start_time <= finish_time, \
            "{} {} {} {} {}".format(submit_time
                                   ,launch_time
                                   ,last_mapper_finish_time
                                   ,first_reducer_start_time
                                   ,finish_time)
        assert num_maps >= 0 and num_reduces >= 0
        assert num_susp >= 0 and num_resumed >= 0
        self.summary_creation = summary_creation
        self.job_id = job_id
        self.submit_time = submit_time/1000.
        self.launch_time = launch_time/1000.
        self.last_mapper_finish_time = last_mapper_finish_time/1000.
        self.first_reducer_start_time = first_reducer_start_time/1000.
        self.finish_time = finish_time/1000.
        self.num_maps = num_maps
        self.num_slots_per_map = num_slots_per_map
        self.num_reduces = num_reduces
        self.num_slots_per_reduce = num_slots_per_reduce
        self.num_susp = num_susp
        self.num_resumed = num_resumed

    def __str__(self):
        return ("JobSummary(id:{},".format(self.job_id)
                         + "submit:{},".format(self.submit_time)
                         + "launch:{},".format(self.launch_time)
                         + "lastMap:{},".format(self.last_mapper_finish_time)
                         + "firstRed:{},".format(self.first_reducer_start_time)
                         + "finish:{},".format(self.finish_time)
                         + "numMaps:{},".format(self.num_maps)
                         + "numReds:{},".format(self.num_reduces)
                         + "susp:{},".format(self.num_susp)
                         + "resumed:{}".format(self.num_resumed)
                         + ")")

    def waitTime(self):
        return self.launch_time - self.submit_time

    def sojournTime(self):
        return self.finish_time - self.submit_time

    def execTime(self):
        return self.finish_time - self.launch_time

    def mapTime(self):
        return self.last_mapper_finish_time - self.launch_time

    def reduceTime(self):
        return self.finish_time - self.first_reducer_start_time

    def betweenMapReduceTime(self):
        return self.first_reducer_start_time - self.last_mapper_finish_time

def build_re_for_jobsummary():
    re_str = '.'.join(['^(?P<timestamp>\d+) INFO org.apache.hadoop.mapred'
                      ,'JobInProgress\$JobSummary#logJobSummary\(\d+\) '])
    fields = ['jobId=(?P<jobId>\w+)']
    for s in ['submitTime','launchTime','mapFinishTime','reduceStartTime'
             ,'finishTime','numMaps','numSlotsPerMap','numReduces'
             ,'numSlotsPerReduce','numSuspendedReduces','numResumedReduces']:
        fields.append('{}=(?P<{}>\d+)'.format(s,s))
    re_str += ','.join(fields)
    return re.compile(re_str)        

def parseJobSummaryLine(line):
    job_summary = None
    if not line:
        return job_summary
    matchObj = build_re_for_jobsummary().match(line)
    if matchObj:
        summary_creation = matchObj.group("timestamp")
        job_id = matchObj.group("jobId")
        submit_time = long(matchObj.group("submitTime"))
        launch_time = long(matchObj.group("launchTime"))
        last_mapper_finish_time = long(matchObj.group('mapFinishTime'))
        first_reducer_start_time = long(matchObj.group('reduceStartTime'))
        finish_time = long(matchObj.group("finishTime"))

        num_maps = int(matchObj.group("numMaps"))
        num_slots_per_map = int(matchObj.group("numSlotsPerMap"))
        num_reduces = int(matchObj.group("numReduces"))
        num_slots_per_reduce = int(matchObj.group("numSlotsPerReduce"))

        num_susp = int(matchObj.group('numSuspendedReduces'))
        num_resumed = int(matchObj.group('numResumedReduces'))

        job_summary = JobSummary(summary_creation
                               , job_id
                               , submit_time
                               , launch_time
                               , last_mapper_finish_time
                               , first_reducer_start_time
                               , finish_time
                               , num_maps
                               , num_slots_per_map
                               , num_reduces
                               , num_slots_per_reduce
                               , num_susp
                               , num_resumed)
    else:
        print("WARN: line '{}' doesn't match a job summary line".format(line))
    return job_summary
