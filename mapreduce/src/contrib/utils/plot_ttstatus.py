from collections import Counter
import sys
import os
import re
import tempfile


class Parser(object):
    #140744 INFO org.apache.hadoop.mapred.JobTracker#createTaskEntry(1853) Adding task (MAP) 'attempt_fakejt_0001_m_000009_0' to tip task_fakejt_0001_m_000009, for tracker 'tracker_machine_0:localhost/127.0.0.1:10000'
    adding_reg_str = '''^(?P<timestamp>\d+) INFO org.apache.hadoop.mapred.JobTracker#createTaskEntry\(\d+\) Adding task \((?P<taskType>\w+)\) \'(?P<taskAttemptId>\w+)\' to tip \w+\, for tracker \'(?P<trackerName>\w+):[\w\/\.\:]+\''''
    #remove_reg_str = '''^(?P<timestamp>\d+) INFO org.apache.hadoop.mapred.JobTracker#removeTaskEntry\(\d+\) Removing task \'(?P<taskAttemptId>\w+)\''''
    completed_reg_str = '''^(?P<timestamp>\d+) INFO org.apache.hadoop.mapred.JobInProgress#completedTask\(\d+\) Task \'(?P<taskAttemptId>\w+)\' has completed \w+ successfully.$'''
    #suspend_reg_str = '''^(?P<timestamp>\d+) INFO org.apache.hadoop.mapred.TaskInProgress#setSuspended\(\d+\) setSuspended\((?P<taskAttemptId>\w+)\) succeed'''
    suspend_reg_str = '''^(?P<timestamp>\d+) INFO org.apache.hadoop.mapred.TaskInProgress#suspendTaskAttempt\(\d+\) set (?P<taskAttemptId>\w+) to be suspended for the next TT heartbeat'''
    #resume_reg_str = '''^(?P<timestamp>\d+) INFO org.apache.hadoop.mapred.TaskInProgress#setResumed\(\d+\) setResumed\((?P<taskAttemptId>\w+)\) succeed'''
    resume_reg_str = '''^(?P<timestamp>\d+) INFO org.apache.hadoop.mapred.TaskInProgress#resumeTaskAttempt\(\d+\) set (?P<taskAttemptId>\w+) to be resumed for the next TT heartbeat'''

    kill_reg_str = '''^(?P<timestamp>\d+) INFO org.apache.hadoop.mapred.TaskInProgress#killTask\(\d+\).*'(?P<taskAttemptId>\w+)'.*'''

    TASKSTYPES = ('MAP', 'REDUCE')

    def __init__(self, filename):
        if not os.path.exists(filename):
            raise IOError("File %s does not exists" % filename)
        self.filename = filename
        self.add_rex = re.compile(Parser.adding_reg_str)
        self.completed_rex = re.compile(Parser.completed_reg_str)
        self.suspend_rex = re.compile(Parser.suspend_reg_str)
        self.kill_rex = re.compile(Parser.kill_reg_str)
        self.resume_rex = re.compile(Parser.resume_reg_str)

    def __taskAttemptId2Info(self, taskAttemptId):
        tok = taskAttemptId.split("_")
        jobName = "_".join(tok[1:3])
        jobId = int(tok[2])

        if tok[3] == "m":
            taskType = "MAP"
        elif tok[3] == "r":
            taskType = "REDUCE"
        else:
            raise Exception("Invalid String")

        return jobName, jobId, taskType

    def __parse_line(self, line):
        matchObj = self.add_rex.match(line) or self.resume_rex.match(line)

        if matchObj:
            t = int(matchObj.group("timestamp"))
            taskAttemptId = matchObj.group("taskAttemptId")
            jobName, jobId, taskType = self.__taskAttemptId2Info(matchObj.group("taskAttemptId"))
            assert (taskType in Parser.TASKSTYPES)
            print "#", t, jobName, jobId, taskType, taskAttemptId, "ADDING", (self.resume_rex.match(line) is not None and "RESUMED" or "")
            return t, jobId, taskType, +1

        matchObj = (self.completed_rex.match(line) or self.suspend_rex.match(line)
                    or self.kill_rex.match(line))
        if matchObj:
            t = int(matchObj.group("timestamp"))
            taskAttemptId = matchObj.group("taskAttemptId")
            jobName, jobId, taskType = self.__taskAttemptId2Info(matchObj.group("taskAttemptId"))
            print "#", t, jobName, jobId, taskType, taskAttemptId, "REMOVING",  (self.suspend_rex.match(line) is not None and "SUSPENDED" or "")
            return t, jobId, taskType, -1

        return None


    def advanceToNextTimestamp(self):
        current_t = -1
        # mapsPerJob and reducesPerJob contain the number of maps/reduces for each job in each timestamp: JobId --> num_tasks
        mapsPerJob = Counter()
        reducesPerJob = Counter()
        with open(self.filename) as f:
            for line in f:
                r = self.__parse_line(line)
                if r:
                    t, jobId, taskType, delta = r
                    assert( t >= current_t )
                    if t > current_t:
                        if current_t != -1:
                            yield current_t, mapsPerJob, reducesPerJob
                        current_t = t
                    if taskType == "MAP":
                        mapsPerJob[jobId]+=delta
                    elif taskType == "REDUCE":
                        reducesPerJob[jobId]+=delta
                    else:
                        raise Exception()

        yield current_t, mapsPerJob, reducesPerJob

class JobsOccupation(object):
    def __init__(self):
        self.data = {}
        self.job_prev_time = {}
        self.prev_t = -1
    
    def add_data(time, cnt, job):
        slots = cnt.get(job, 0)
        if not (self.data.has_key(job) and self.data[job][prev_time[job]]):
            self.data[job][time] = slots
            self.job_prev_time[job] = time


class Square(object):
    """
     y1  -----------
         |         |
     y0  |_________|
        x0         x1
    """

    def __init__(self, x0, y0, y1):
        self.x0 = x0
        self.y0 = y0
        self.y1 = y1
        
        self.x1 = None
    
    def finish(self, x1):
        self.x1 = x1
    
    def isFinished(self):
        return self.x1 is not None
    
    def center(self):
        assert(self.x1 is not None)
        return ( float(self.x1+self.x0)/2, float(self.y1+self.y0)/2)

    def __str__(self):
        return "".join([str(i) for i in "(x0:", self.x0, ".y0=", self.y0, " - x1=", self.x1, ".y1=", self.y1, ")"])

    def getArea(self):
        return (self.x1-self.x0) * (self.y1-self.y0)


    def isContiguousTo(self, otherSquare):
        if not (self.y1<otherSquare.y0 or self.y0>otherSquare.y1):
            return True
        else:
            #print "False"
            return False




def main(taskType):
    #NUMJOBS = int(sys.argv[2])
    prev_t = None

    x = []
    yy = {}

    prev_line = None
    started_square = {}
    squares = {}

    parser = Parser(sys.argv[1])
    tmpdatfilename = tempfile.mkstemp(suffix="plt")[1]
    with open(tmpdatfilename, "w") as fw:
        for t, mapsPerJob, reducesPerJob in parser.advanceToNextTimestamp():
            if prev_t == t:
                sys.stderr.write("ERRORE, dupplicate timestamp: %s" % t)
                raise Exception()

            if (taskType == "MAP"):
                tasksPerJob = mapsPerJob
            elif(taskType == "REDUCE"):
                tasksPerJob = reducesPerJob
            if(prev_t!=None):
                x.append(prev_t)

            line_to_print = []

            prev_num_slots = 0 
            t_str = "%.2f" % t
            line_to_print.append("0")
            #for i in range(NUMJOBS):
            for jobId, num_slots in tasksPerJob.iteritems():
                i = jobId
                num_slots = int(num_slots)

                #LABEL
                if num_slots != 0:
                    if started_square.get(i, False) == False:
                        #print "%.2f\tStarting square %d" % (prev_t, i)
                        squares.setdefault(i, []).append(Square(t, prev_num_slots, prev_num_slots + num_slots))
                        started_square[i] = True
                    elif squares[i][-1].y0 != prev_num_slots or squares[i][-1].y1 != (prev_num_slots + num_slots):
                        squares[i][-1].finish(t)
                        squares[i].append(Square(t, prev_num_slots, prev_num_slots + num_slots))
                        started_square[i] = True
                else:
                    if started_square.get(i, False) == True:
                        #print "%.2f\tStop square %d" % (prev_t, i)
                        squares[i][-1].finish(t)
                        started_square[i] = False
                
                prev_num_slots+= num_slots
                line_to_print.append("%d" % prev_num_slots)
                #yy.setdefault(i+1, []).append(num_slots)
            #print
            thisline = " ".join(line_to_print)
            if prev_line != None:
                #print "%.5f" % (prev_t-0.001), prev_line
                fw.write("%.5f" % (t-0.01))
                fw.write(" ")
                fw.write(prev_line)
                fw.write("\n")
            #print t_str, thisline
            fw.write(t_str)
            fw.write(" ")
            fw.write(thisline)
            fw.write("\n")
            prev_line = thisline
            prev_t = t

    for i in squares.keys():
        if not squares[i][-1].isFinished():
            squares[i][-1].finish(prev_t)

    yy[0] = [0] * len(x)

    #fig = figure()
    #ax = fig.add_subplot(111)
    #for i in range(NUMJOBS):
        #ax.fill_between(x, yy[i+1], yy[i]) 
    #show()
    tmptextdatfilename=tempfile.mkstemp(suffix="pltx")[1]
    with open(tmptextdatfilename, "w") as fwt:
        contiguous_squares = {}
        
        for i in squares.keys(): # i = jobId
            prev_square = None
            for square in squares[i]:
                if prev_square is None:
                    contiguous_squares.setdefault(i, []).append(square)
                elif (prev_square is not None and not prev_square.isContiguousTo(square)):
                    ignore, biggestSq = max([(sq.getArea(), sq) for sq in contiguous_squares.get(i, [])])
                    x, y = biggestSq.center()
                    fwt.write("%s %.2f %.2f\n" % (i+1, x, y))
                    contiguous_squares[i] = [square]
                else:
                    contiguous_squares.setdefault(i, []).append(square)

                prev_square = square

            if contiguous_squares.has_key(i):
                ignore, biggestSq = max([(sq.getArea(), sq) for sq in contiguous_squares.get(i, [])])
                x, y = biggestSq.center()
                fwt.write("%s %.2f %.2f\n" % (i+1, x, y))


    print "set nokey"
    #print "set yrange [0:210]"
    #print "set style fill pattern 2 border"
    print "set style fill transparent solid 0.2 border 1"
    print "set border 15"
    plot_line = []
    #for i in range(1,51):
    for i in mapsPerJob.keys():
        i = i+1
        plot_line.append('"%s" using 1:%d:%d w filledcurve title "job%03d"' % (tmpdatfilename, i+1, i+2, i+1))

    plot_line.append('"%s" using 2:3:1 w labels' % tmptextdatfilename)

    print "plot", ",".join(plot_line)



if __name__ == "__main__":
    main(sys.argv[2])
