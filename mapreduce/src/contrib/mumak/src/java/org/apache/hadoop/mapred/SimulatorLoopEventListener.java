package org.apache.hadoop.mapred;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class SimulatorLoopEventListener implements SimulatorEventListener {

	private static final Log LOG = LogFactory.getLog(SimulatorLoopEventListener.class);
	
	private List<SimulatorEvent> events;
	private long interval;
	private long lastEventTime;

	public SimulatorLoopEventListener(long interval) {
		this.interval = interval;
		this.lastEventTime = 0;
		this.events = new LinkedList<SimulatorEvent>();
	}
	
	private SimulatorEvent nextLoopEvent() {
		//System.out.println("Next HFSP update: " + (this.lastEventTime + this.interval));
		return new SimulatorLoopEvent(this, this.lastEventTime + this.interval);
	}
	
	@Override
	public List<SimulatorEvent> init(long when) throws IOException {
		this.events.clear();
		this.lastEventTime = when;
		return Collections.singletonList(this.nextLoopEvent());
	}

	@Override
	public List<SimulatorEvent> accept(SimulatorEvent event) throws IOException {
		this.lastEventTime = event.getTimeStamp();
		this.events.clear();
		this.events.addAll(this.fireLoopEvent(event));
		this.events.add(this.nextLoopEvent());
		return events;
	}

	public abstract List<SimulatorEvent> fireLoopEvent(SimulatorEvent event);
}
