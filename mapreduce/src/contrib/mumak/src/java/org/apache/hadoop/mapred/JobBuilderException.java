package org.apache.hadoop.mapred;

public class JobBuilderException extends Exception {

	public JobBuilderException(String string) {
		super(string);
	}

}
