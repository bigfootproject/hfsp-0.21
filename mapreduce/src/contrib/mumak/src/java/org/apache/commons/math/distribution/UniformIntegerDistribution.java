/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.commons.math.distribution;

import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.AbstractIntegerDistribution;

/**
 * Implementation of the uniform integer distribution.
 *
 * @see <a href="http://en.wikipedia.org/wiki/Uniform_distribution_(discrete)"
 * >Uniform distribution (discrete), at Wikipedia</a>
 *
 * @version $Id: UniformIntegerDistribution.java 1244107 2012-02-14 16:17:55Z erans $
 * @since 3.0
 */
public class UniformIntegerDistribution extends AbstractIntegerDistribution {
    /** Serializable version identifier. */
    private static final long serialVersionUID = 20120109L;

    /** Lower bound (inclusive) of this distribution. */
    private int lower;

    /** Upper bound (inclusive) of this distribution. */
    private int upper;

    @Deprecated
    public UniformIntegerDistribution() { }
    
    /**
     * Creates a new uniform integer distribution using the given lower and
     * upper bounds (both inclusive).
     *
     * @param lower Lower bound (inclusive) of this distribution.
     * @param upper Upper bound (inclusive) of this distribution.
     * @throws NumberIsTooLargeException if {@code lower >= upper}.
     */
    public UniformIntegerDistribution(int lower, int upper) throws MathException {
        if (lower >= upper) {
            throw new MathException(
            		"lower bound (" + lower 
            		+ ") must be strictly less than upper bound (" + upper + ")");
        }
        this.setLower(lower);
        this.setUpper(upper);
    }

    /** {@inheritDoc} */
    public double probability(int x) {
        if (x < getLower() || x > getUpper()) {
            return 0;
        }
        return 1.0 / (getUpper() - getLower() + 1);
    }

    /** {@inheritDoc} */
    public double cumulativeProbability(int x) {
        if (x < getLower()) {
            return 0;
        }
        if (x > getUpper()) {
            return 1;
        }
        return (x - getLower() + 1.0) / (getUpper() - getLower() + 1.0);
    }

    /**
     * {@inheritDoc}
     *
     * For lower bound {@code lower} and upper bound {@code upper}, the mean is
     * {@code 0.5 * (lower + upper)}.
     */
    public double getNumericalMean() {
        return 0.5 * (getLower() + getUpper());
    }

    /**
     * {@inheritDoc}
     *
     * For lower bound {@code lower} and upper bound {@code upper}, and
     * {@code n = upper - lower + 1}, the variance is {@code (n^2 - 1) / 12}.
     */
    public double getNumericalVariance() {
        double n = getUpper() - getLower() + 1;
        return (n * n - 1) / 12.0;
    }

    /**
     * {@inheritDoc}
     *
     * The lower bound of the support is equal to the lower bound parameter
     * of the distribution.
     *
     * @return lower bound of the support
     */
    public int getSupportLowerBound() {
        return getLower();
    }

    /**
     * {@inheritDoc}
     *
     * The upper bound of the support is equal to the upper bound parameter
     * of the distribution.
     *
     * @return upper bound of the support
     */
    public int getSupportUpperBound() {
        return getUpper();
    }

    /**
     * {@inheritDoc}
     *
     * The support of this distribution is connected.
     *
     * @return {@code true}
     */
    public boolean isSupportConnected() {
        return true;
    }

		@Override
		protected int getDomainLowerBound(double arg0) {
			return getLower();
		}

		@Override
		protected int getDomainUpperBound(double arg0) {
			return getUpper();
		}

		public int getLower() {
			return lower;
		}

		public void setLower(int lower) {
			this.lower = lower;
		}

		public int getUpper() {
			return upper;
		}

		public void setUpper(int upper) {
			this.upper = upper;
		}
}