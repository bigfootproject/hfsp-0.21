package org.apache.hadoop.mapred;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class SimulatorHFSPUpdateThreadEvent extends SimulatorLoopEventListener {

	private HFSPScheduler scheduler;

	public SimulatorHFSPUpdateThreadEvent(HFSPScheduler scheduler
																					 , long interval) {
		super(interval);
		this.scheduler = scheduler;
	}

	@Override
	public List<SimulatorEvent> fireLoopEvent(SimulatorEvent event) {
		try {
			this.scheduler.update();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Collections.emptyList();
	}

}
