package org.apache.hadoop.mapred;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.AbstractContinuousDistribution;
import org.apache.commons.math.distribution.AbstractIntegerDistribution;
import org.apache.commons.math.distribution.ConstantRandomValue;
import org.apache.commons.math.distribution.Distribution;
import org.apache.commons.math.distribution.NormalDistributionImpl;
import org.apache.commons.math.distribution.UniformIntegerDistribution;
import org.apache.commons.math.random.JDKRandomGenerator;
import org.apache.commons.math.random.RandomAdaptor;
import org.apache.commons.math.random.RandomGenerator;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.JobACL;
import org.apache.hadoop.mapreduce.JobID;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.TaskType;
import org.apache.hadoop.mapreduce.jobhistory.HistoryEvent;
import org.apache.hadoop.mapreduce.jobhistory.JobFinishedEvent;
import org.apache.hadoop.mapreduce.jobhistory.JobInitedEvent;
import org.apache.hadoop.mapreduce.jobhistory.JobSubmittedEvent;
import org.apache.hadoop.mapreduce.jobhistory.MapAttemptFinishedEvent;
import org.apache.hadoop.mapreduce.jobhistory.ReduceAttemptFinishedEvent;
import org.apache.hadoop.mapreduce.jobhistory.TaskAttemptFinishedEvent;
import org.apache.hadoop.mapreduce.jobhistory.TaskAttemptStartedEvent;
import org.apache.hadoop.mapreduce.jobhistory.TaskFinishedEvent;
import org.apache.hadoop.mapreduce.jobhistory.TaskStartedEvent;
import org.apache.hadoop.security.authorize.AccessControlList;
import org.apache.hadoop.tools.rumen.ClusterStory;
import org.apache.hadoop.tools.rumen.JobBuilder;
import org.apache.hadoop.tools.rumen.LoggedJob;
import org.apache.hadoop.tools.rumen.LoggedTask;
import org.apache.hadoop.tools.rumen.LoggedTaskAttempt;
import org.apache.hadoop.tools.rumen.MachineNode;
import org.apache.hadoop.tools.rumen.RandomSeedGenerator;
import org.apache.hadoop.tools.rumen.Pre21JobHistoryConstants.Values;

public class SimulatorJob extends LoggedJob {

	final static Map<String, Class<? extends Distribution>> distributions = new HashMap<String, Class<? extends Distribution>>();
	static {
		distributions.put("NormalDistributionImpl",
				NormalDistributionImpl.class);
		distributions.put("UniformIntegerDistribution",
				UniformIntegerDistribution.class);
		distributions.put("ConstantRandomValue", ConstantRandomValue.class);
	}

	private String mapDurationDistribution;
	private Map<String, String> mapDistributionParameters;
	private String reduceDurationDistribution;
	private Map<String, String> reduceDistributionParameters;
	private String jobTrackerName;
	private int jid_number;
	private JobID jid;
	
	private boolean template = false;

	public SimulatorJob() {
	}

	public String getMapDurationDistribution() {
		return mapDurationDistribution;
	}

	public void setMapDurationDistribution(String mapDurationDistribution) {
		// distributions.
		this.mapDurationDistribution = mapDurationDistribution;
	}

	public Map<String, String> getMapDistributionParameters() {
		return mapDistributionParameters;
	}

	public void setMapDistributionParameters(
			Map<String, String> mapDistributionParameters) {
		this.mapDistributionParameters = mapDistributionParameters;
	}

	public String getReduceDurationDistribution() {
		return reduceDurationDistribution;
	}

	public void setReduceDurationDistribution(String reduceDurationDistribution) {
		this.reduceDurationDistribution = reduceDurationDistribution;
	}

	public Map<String, String> getReduceDistributionParameters() {
		return reduceDistributionParameters;
	}

	public void setReduceDistributionParameters(
			Map<String, String> reduceDistributionParameters) {
		this.reduceDistributionParameters = reduceDistributionParameters;
	}
	
	public boolean isTemplate() {
		return template;
	}

	public void setTemplate(boolean template) {
		this.template = template;
	}

	public String toString() {
		return "SimpleJob: (" + (template?"TEMPLATE ":"") + "totalMaps = " + getTotalMaps() + ", "
				+ "totalReduces = " + getTotalReduces() + ","
				+ "mapDistribution = " + mapDurationDistribution + ", "
				+ "reduceDistribution " + reduceDurationDistribution + ", "
				+ "mapDistributionParams = " + mapDistributionParameters + ","
				+ "reduceDistributionParams " + reduceDistributionParameters
				+ ")";

	}

	public LoggedJob build(int counter, long seed, ClusterStory cluster, JobConf jobConf) throws JobBuilderException,
			MathException {
		RandomGenerator generator = new JDKRandomGenerator();
		generator.setSeed(seed);
		jobTrackerName = "fakejt";
		jid = getJobID(jobTrackerName, counter);
		jid_number = counter;
		this.setJobID(jid.toString());
		if(getJobName() == null)
			this.setJobName("job" + counter);
		
		if (getUser()==null)
			this.setUser("userhadoop");
		

		JobBuilder jb = new JobBuilder(jid.toString());

		jb.process(generateJobSubmissionEvent());
		jb.process(generateJobStartEvent());
		generateRandomMapTasks(jb, generator, jid, cluster, jobConf);
		generateRandomReduceTasks(jb, generator, jid, cluster, jobConf);
		jb.process(generateJobFinishEvent());

		
		
		return jb.build();
	}

	private JobID getJobID(String jt, int counter) {
		return new JobID(jt, counter);
	}

	private HistoryEvent generateJobSubmissionEvent() {
		// JobSubmittedEvent ev = new JobSubmittedEvent(jid, getJobName(),
		// getUser(), getSubmitTime(), jobConfPath(), getJobACLs());
		Map<JobACL, AccessControlList> jobACLs = new HashMap<JobACL, AccessControlList>();
		return new JobSubmittedEvent(jid, getJobName(), getUser(),
				getSubmitTime(), "", jobACLs);
	}

	private HistoryEvent generateJobStartEvent() {
		String status = JobStatus.State.SUCCEEDED.toString();
		return new JobInitedEvent(jid, getLaunchTime(), getTotalMaps(),
				getTotalReduces(), status);

	}

	private HistoryEvent generateJobFinishEvent() {
		int finishedMaps = getTotalMaps();
		int finishedReduces = getTotalReduces();
		int failedMaps = 0;
		int failedReduces = 0;
		Counters counters = new Counters();

		JobFinishedEvent ev = new JobFinishedEvent(jid, getFinishTime(),
				finishedMaps, finishedReduces, failedMaps, failedReduces,
				counters, counters, counters);
		return ev;
	}

	// private void generateReduceTasks(JobBuilder jb) {
	// // TODO Auto-generated method stub
	// ReduceAttemptFinishedEvent ev;
	// if (getReduceTasks().isEmpty() && getTotalReduces() > 0) {
	// Distribution d = initDistribution(mapDurationDistribution,
	// mapDistributionParameters);
	// generateRandomTasks(totalReduces, generator, d, reduceTaskList);
	// }
	// }

	private void generateRandomMapTasks(JobBuilder jb,
			RandomGenerator generator, JobID jid, ClusterStory cluster, JobConf jobConf) throws JobBuilderException,
			MathException {
		if (getMapTasks().isEmpty() && getTotalMaps() > 0) {
			Distribution d = initDistribution(mapDurationDistribution,
					mapDistributionParameters);
			// generateRandomTasks(totalMaps, generator, d,
			// mapTaskList);v---------------------------------------------
			generateRandomTasks(generator, d, TaskType.MAP, jb, jid, cluster, jobConf);
		}
	}

	private void generateRandomReduceTasks(JobBuilder jb,
			RandomGenerator generator, JobID jid, ClusterStory cluster, JobConf jobConf) throws JobBuilderException,
			MathException {
		if (getReduceTasks().isEmpty() && getTotalReduces() > 0) {
			Distribution d = initDistribution(reduceDurationDistribution,
					reduceDistributionParameters);
			generateRandomTasks(generator, d, TaskType.REDUCE, jb, jid, cluster, jobConf);
		}
	}

	private void generateRandomTasks(RandomGenerator generator, Distribution d,
			TaskType type, JobBuilder jb, JobID jid, ClusterStory cluster, JobConf jobConf) throws MathException,
			JobBuilderException {
		int totalTasks = (type == TaskType.MAP) ? getTotalMaps()
				: getTotalReduces();

		for (int i = 0; i < totalTasks; i++) {
			double taskLen;
			if (d instanceof AbstractContinuousDistribution) {
				AbstractContinuousDistribution acd = (AbstractContinuousDistribution) d;
				taskLen = acd.inverseCumulativeProbability(generator
						.nextDouble());
			} else if (d instanceof AbstractIntegerDistribution) {
				AbstractIntegerDistribution aid = (AbstractIntegerDistribution) d;
				taskLen = aid.inverseCumulativeProbability(generator
						.nextDouble());
			} else {
				throw new JobBuilderException("The specified distribution(" + d
						+ ") cannot be inverted ");
			}

			TaskID tid = new TaskID(jid, type, i);
			TaskAttemptID attemptId = new TaskAttemptID(jobTrackerName,
					jid_number, type, i, 0);
			
			String splitLocationsString = generateSplitLocations(generator, cluster, 3);
			//System.err.println("TODO: add replication factor in configuration. now it is 3");
			String trackerName = splitLocationsString.split(",")[0];
			int httpPort = 1123;
			String status = "SUCCESS"; // TaskStatus.State.SUCCEEDED.toString();
			Counters counters = new Counters();
			long finishTime = (long) (this.getSubmitTime() + taskLen);
			
			jb.process(new TaskStartedEvent(tid, this.getSubmitTime(), type,
					splitLocationsString));
			jb.process(new TaskAttemptStartedEvent(attemptId, type, this
					.getSubmitTime(), trackerName, httpPort));
			jb.process(new TaskFinishedEvent(tid, finishTime, type, status,
					counters));
			if (type == TaskType.MAP) {
				jb.process(new MapAttemptFinishedEvent(attemptId, type, status,
						finishTime, finishTime, trackerName, status, counters));
//			System.out.println(attemptId + ": length = " + taskLen
//					+ " => finishTime = " + finishTime);
			} else if (type == TaskType.REDUCE) {
				double shufflepercent = Double.parseDouble(jobConf.get("mumak.reduces.shufflepercent", "0.3333"));
				double mergepercent = Double.parseDouble(jobConf.get("mumak.reduces.mergepercent", "0.3333"));
				
				long shuffleFinishTime = (long) (this.getSubmitTime() + taskLen*shufflepercent);
				long sortFinishTime = (long) (this.getSubmitTime() + taskLen*(mergepercent + shufflepercent));
				//System.out.println(attemptId + ": shufflepercent = " + shufflepercent + "; mergepercent = " + mergepercent + "; tasklen = " + taskLen);
				jb.process(new ReduceAttemptFinishedEvent(attemptId, type,
						status, shuffleFinishTime, sortFinishTime,
						finishTime, trackerName, status, counters));
			}
			jb.process(new TaskAttemptFinishedEvent(attemptId, type, status,
					finishTime, trackerName, status, counters));
		}

	}

	/**
	 * @param generator 
	 * @param cluster 
	 * @return
	 */
	private String generateSplitLocations(RandomGenerator generator, ClusterStory cluster, int numSplits) {
		Random random = RandomAdaptor.createAdaptor(generator);
		MachineNode[] machines = cluster.getRandomMachines(numSplits, random);
//		String[] hosts = new String[machines.length];
		StringBuilder sb = new StringBuilder();
	    for (int j = 0; j < machines.length; ++j) {
//	    	hosts[j] = machines[j].getName();
	    	if (j>0)
	    		sb.append(",");
	    	sb.append("/127.0.0.1/" + machines[j].getName());
	    }
	    
		return sb.toString();
	}

	private Distribution initDistribution(String distributionName,
			Map<String, String> distributionParams) throws JobBuilderException {

		Class c = distributions.get(distributionName);
		if (c == null) {
			throw new JobBuilderException("Distribution " + distributionName
						+ " doesn't exist");
		}
		Distribution d;
		try {
			d = (Distribution) c.newInstance();
			Map<String, Object> properties = BeanUtils.describe(d);
			for (Map.Entry<String, String> entry : distributionParams.entrySet()) {
				String methodName = entry.getKey();
				String value = entry.getValue();
				BeanUtils.setProperty(d, methodName, value);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
			throw new JobBuilderException("Impossible to init distribution "
					+ distributionName + " -- " + e.toString());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new JobBuilderException("Impossible to init distribution "
					+ distributionName + " -- " + e.toString());
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			throw new JobBuilderException("Impossible to init distribution "
					+ distributionName + " -- " + e.toString());
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			throw new JobBuilderException("Impossible to init distribution "
					+ distributionName + " -- " + e.toString());
		}
		return d;
	}
}
