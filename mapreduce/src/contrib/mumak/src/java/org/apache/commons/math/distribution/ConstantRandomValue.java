package org.apache.commons.math.distribution;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.math.MathException;

public class ConstantRandomValue extends AbstractContinuousDistribution {

	private double value;

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public ConstantRandomValue() {}
	
	public ConstantRandomValue(double value) {
		this.value = value;
	}
	
	@Override
	public double inverseCumulativeProbability(double x) {
		return this.value;
	}

	@Override
	public double cumulativeProbability(double p) throws MathException {
		throw new NotImplementedException();
	}

	@Override
	protected double getDomainLowerBound(double arg0) {
		throw new NotImplementedException();
	}

	@Override
	protected double getDomainUpperBound(double arg0) {
		throw new NotImplementedException();
	}

	@Override
	protected double getInitialDomain(double arg0) {
		throw new NotImplementedException();
	}

}
