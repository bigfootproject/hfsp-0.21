package org.apache.hadoop.mapred;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.math.MathException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.tools.rumen.ClusterStory;
import org.apache.hadoop.tools.rumen.DefaultOutputter;
import org.apache.hadoop.tools.rumen.JobStoryProducer;
import org.apache.hadoop.tools.rumen.LoggedJob;
import org.apache.hadoop.tools.rumen.LoggedNetworkTopology;
import org.apache.hadoop.tools.rumen.MachineNode;
import org.apache.hadoop.tools.rumen.Outputter;
import org.apache.hadoop.tools.rumen.RandomSeedGenerator;
import org.apache.hadoop.tools.rumen.ZombieCluster;
import org.apache.hadoop.util.ToolRunner;
import org.mortbay.log.Log;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

public class SimulatorEngine2 extends SimulatorEngine {

	public enum ClusterInitType {
		FLATTOPOLOGY, LEGACYMUMAK;
	}

	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {
		System.out.println("Mumak Next");
		int res = ToolRunner.run(new Configuration(), new SimulatorEngine2(),
				args);
		System.exit(res);
	}

	ClusterInitType clusterInitType = ClusterInitType.LEGACYMUMAK;
	int machineNumber = -1;
	private Set<String> inputFiles = null;
	private LinkedList<SimulatorJob> jobList;

	@Override
	public int run(String[] args) throws Exception {
		this.parseParameters(args);
		if (!inputFiles.isEmpty()) {
			this.parseInputFiles(inputFiles);
			//FIXME Move cluster init
			JobConf jobConf = initJobConf();	
			ZombieCluster cluster = createCluster(jobConf);
			this.generateLoad(cluster);
		}
		if (machineNumber < 1)
			throw new RuntimeException("The number of machine cannot be " 
																	+ machineNumber);
		try {
			run();
			return 0;
		} finally {
			if (jt != null) {
				jt.getTaskScheduler().terminate();
			}
		}
	}
	
	private void parseInputFiles(Set<String> inputFiles) 
																					throws FileNotFoundException
																							  , IllegalAccessException
																							  , InvocationTargetException
																							  , InvalidJobException {
		for (String inputFile : inputFiles) {
			this.parseInputFile(inputFile);
		}
	}
	
	private void parseInputFile(String inputFile)
																					throws IllegalAccessException
																							  , FileNotFoundException
																							  , InvocationTargetException
																							  , InvalidJobException {
		InputStream input = new FileInputStream(new File(inputFile));
		jobList = new LinkedList<SimulatorJob>();
		Yaml yaml = new Yaml();
		
		Map<String,Object> root = (Map<String,Object>) yaml.load(input);
		for (Entry<String,Object> e : root.entrySet()) {
			String key = e.getKey();
			Object val = e.getValue();
			if (key.equals("cluster")) {
				if (val instanceof LinkedHashMap) {
					for (Object rcp : ((LinkedHashMap) val).entrySet()) {
						Entry cp = (Entry) rcp;
						String cpKey = cp.getKey().toString();
						String cpVal = cp.getValue().toString();
						if (cpKey.equals("topology")) {
							//System.out.println("topology: " + cpVal + " (ignoring it)");
							this.clusterInitType = ClusterInitType.valueOf(
																										ClusterInitType.class
																									, cpVal);
						} else
						if (cpKey.equals("machineNumber")) {
							if (machineNumber == -1) {
								machineNumber = Integer.parseInt(cpVal);
							} else {
								System.out.println("machineNumber is already set," +
										" ignoring new value: " + cpVal);
							}
						} else
						if (cpKey.equals("mapSlots")) {
							if (getConf().getInt("mapreduce.tasktracker.map.tasks.maximum"
														 , -1) == -1) {
								getConf().setInt("mapreduce.tasktracker.map.tasks.maximum"
															 , Integer.parseInt(cpVal));	
							} else {
								System.out.println("mapSlots is already set," +
										" ignoring new value: " + cpVal);
							}
						} else
						if (cpKey.equals("reduceSlots")) {
							if (getConf().getInt("mapreduce.tasktracker.reduce.tasks.maximum"
															 , -1) == -1) {
									getConf().setInt("mapreduce.tasktracker.reduce.tasks.maximum"
																 , Integer.parseInt(cpVal));	
							} else {
									System.out.println("reduceSlots is already set," +
											" ignoring new value: " + cpVal);
							}
						} else {
							System.out.println("Ignoring nknown cluster conf: "
									+ cpKey + ": " + cpVal);
						}
					}
				} else {
					System.out.println("error: cluster value must be a map (it is a "
							+ val.getClass() + ")");
				}
			}
			else if (key.equals("jobs")) {
				List<Map<String,Object>> jobs = (List<Map<String,Object>>) val;
				for (Map<String,Object> j : jobs) {
					SimulatorJob job = new SimulatorJob();
					for (Entry<String,Object> prop : j.entrySet()) {
							if (prop.getValue() instanceof LinkedHashMap) {
								
								// Beanutil converts values to int instead of using string,
								// forcing everything as string
								// FIXME: find a more safe way to work with linkedhashmaps
								LinkedHashMap<String,String> copy = new LinkedHashMap<String,String>();
								for(Object rie : ((LinkedHashMap) prop.getValue()).entrySet()) {
									Entry ie = (Entry) rie;
									String k = ie.getKey().toString();
									String v = ie.getValue().toString();
									//System.out.println(k + ": " + v);
									copy.put(k, v);
								}
								BeanUtils.setProperty(job, prop.getKey(), copy);			
								
							} else {
								BeanUtils.setProperty(job, prop.getKey(), prop.getValue());
							}
					}
					//System.out.println(job + "\n");
					if (job.getSubmitTime() == 0l) {
						throw new InvalidJobException("mumak doesn't support jobs with " +
																					"submit time 0");
					}
					jobList.add(job);
				}
			}
		}
	}

	private void generateLoad(ClusterStory cluster) throws Exception {;
		JobConf jobConf = new JobConf(getConf());
		long masterRandomSeed = jobConf.getLong("mumak.random.seed"
																					 , System.nanoTime());
		System.out.println("Master Seed: " + masterRandomSeed);
		
		long seed = RandomSeedGenerator.getSeed("tasksSeed", masterRandomSeed);

		Outputter<LoggedJob> traceWriter;
		Class<? extends Outputter> clazzTraceOutputter = DefaultOutputter.class;
		traceWriter = clazzTraceOutputter.newInstance();
		traceWriter.init(new Path(this.traceFile)
									 , new Configuration());
		clazzTraceOutputter.newInstance();

		int counter = 0;
		
		for (SimulatorJob job : jobList) {
			LoggedJob ljob = job.build(counter, seed, cluster, jobConf);
			counter++;
			traceWriter.output(ljob);
		}
		
		System.out.println("Read " + counter + " jobs!");

		traceWriter.close();
	}

	@Override
	void parseParameters(String[] args) {
		try {
			Options options = new Options();
			options.addOption("p", "topology", true, "The topology");
			options.addOption("f", "topologyFile", true, "Json topology file");
			options.addOption("M", "machineNumber", true,
					"The number of machines in the cluster (static topology)");
			options.addOption("m", "mapSlots", true,
					"Number of mapSlots  (static topology)");
			options.addOption("r", "reduceSlots", true,
					"Number of reduce slots  (static topology)");
			options.addOption("t", "traceFile", true, "Json trace file");
			options.addOption("j", "jobFile", true, "YAML jobs file");
			options.addOption("h", "help", false, "Print the help");
			options.getRequiredOptions();
			CommandLineParser parser = new PosixParser();
			CommandLine cmd = parser.parse(options, args);

			if (cmd.hasOption(("help"))) {
				StringBuilder builder = new StringBuilder("Usage:");
				builder.append(System.getProperty("line.seperator"));
				for(Object rawOption : options.getOptions()) {
					Option option =  (Option)rawOption;
					builder.append("\t -").append(option.getOpt())
								 .append(" --").append(option.getLongOpt());
					if (option.hasArg()) {
						builder.append(" [args]");
					}
					builder.append(" ").append(option.getDescription())
								 .append(System.getProperty("line.seperator"));
				}
				System.out.println(builder.toString());
				System.exit(0);
			}
			
			//OTHER OPTIONS
			
			if (cmd.hasOption("mapSlots")) {
				getConf().setInt("mapreduce.tasktracker.map.tasks.maximum"
											 , Integer.parseInt(cmd.getOptionValue("mapSlots")));
			}
			if (cmd.hasOption("reduceSlots")) {
				getConf().setInt("mapreduce.tasktracker.reduce.tasks.maximum"
											 , Integer.parseInt(cmd.getOptionValue("reduceSlots")));
			}
			
			// TOPOLOGY
			if (cmd.hasOption("topology")
					&& cmd.getOptionValue("topology").equals("STATIC")) {
				clusterInitType = ClusterInitType.FLATTOPOLOGY;
				if (cmd.hasOption("machineNumber")) {
					machineNumber = Integer.parseInt(cmd
							.getOptionValue("machineNumber"));
				} else 
				if (!cmd.hasOption("jobFile")) {
					throw new IllegalArgumentException("Static topology needs" +
							" machine number");
				}
				topologyFile = cmd.getOptionValue("topologyFile");
			} else {
				if (cmd.hasOption("machineNumber")) {
					System.out.println("machineNumber will be ignored if static topology " +
							"is not provided (-p STATIC)");
				}
				if (cmd.hasOption("topologyFile")) {
					System.out.println("Added topologyFile" + topologyFile);
				} else 
				if (!cmd.hasOption("jobFile")) {
					throw new IllegalArgumentException(
							"Wrong Topology");
				}
			}
			
			// Just one of the two must be defined
			if (!(cmd.hasOption("traceFile") ^ cmd.hasOption("jobFile"))) {
				throw new IllegalArgumentException("You must define the traceFile OR" +
						" the jobFile");
			}

			if (cmd.hasOption("jobFile")) {
				this.inputFiles = new HashSet<String>();
				for (String jf : cmd.getOptionValues("jobFile")) {
					if (!new File(jf).exists()) {
						throw new IllegalArgumentException("jobFile must exist");
					}
					this.inputFiles.add(jf);
				}

				this.traceFile = inputFiles.iterator().next() + ".out";
				Log.debug("Settings the traceFile to " + this.traceFile);
				System.out.println("Settings the traceFile to " + this.traceFile);
			}
			
			// TraceFile
			if (cmd.hasOption("traceFile")) {
				traceFile = cmd.getOptionValue("traceFile");
				if (!new File(this.traceFile).exists()) {
					throw new IllegalArgumentException("traceFile must exist");
				}
			}

		} catch (ParseException e) {
			e.printStackTrace();
			throw new IllegalArgumentException(
					"Parse Exception Usage: java ... SimulatorEngine trace.json topology.json");
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.hadoop.mapred.SimulatorEngine#createCluster(org.apache.hadoop
	 * .mapred.JobConf)
	 */
	@Override
	protected ZombieCluster createCluster(JobConf jobConf) throws IOException {
		if (clusterInitType == ClusterInitType.LEGACYMUMAK) {
			return super.createCluster(jobConf);
		} else if(clusterInitType == ClusterInitType.FLATTOPOLOGY) {
		    int maxMaps = getConf().getInt(
		        "mapreduce.tasktracker.map.tasks.maximum",
		        SimulatorTaskTracker.DEFAULT_MAP_SLOTS);
		    int maxReduces = getConf().getInt(
		        "mapreduce.tasktracker.reduce.tasks.maximum",	    
		      SimulatorTaskTracker.DEFAULT_REDUCE_SLOTS);

		    MachineNode defaultNode = new MachineNode.Builder("default", 2)
		        .setMapSlots(maxMaps).setReduceSlots(maxReduces).build();
		            
		    LoggedNetworkTopology topology = new FlatTopologyGenerator(machineNumber, defaultNode).get();
		    
		    System.out.println("Mumak Cluster flat configuration:"
		    								 + " machineNumber: " + machineNumber
		    								 + " mapSlots: " + maxMaps
		    								 + " reduceSlots: " + maxReduces);
		    		
		    // Setting the static mapping before removing numeric IP hosts.
		    setStaticMapping(topology);
		    if (getConf().getBoolean("mumak.topology.filter-numeric-ips", true)) {
		      removeIpHosts(topology);
		    }
		    ZombieCluster cluster = new ZombieCluster(topology, defaultNode);
//		    System.out.println("CLUSTER: " + cluster.getMachines());
			return cluster;
		}
		else
			throw new IOException("Invalid Cluster Type");
	}

	/* (non-Javadoc)
	 * @see org.apache.hadoop.mapred.SimulatorEngine#getJobList(org.apache.hadoop.mapred.JobConf, org.apache.hadoop.tools.rumen.ZombieCluster, long)
	 */
	@Override
	JobStoryProducer getJobList(JobConf jobConf, ZombieCluster cluster,
			long firstJobStartTime) throws IOException {
		return super.getJobList(jobConf, cluster, firstJobStartTime);
	} 

	static class InvalidJobException extends Exception {

		private static final long serialVersionUID = 4416037163694790570L;

		public InvalidJobException(String why) {
			super(why);
		}
	}
}
