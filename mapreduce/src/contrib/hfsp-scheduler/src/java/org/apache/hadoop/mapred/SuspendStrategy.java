package org.apache.hadoop.mapred;

import org.apache.hadoop.mapred.TaskStatus.State;
import org.apache.hadoop.mapreduce.TaskType;

public class SuspendStrategy implements PreemptionStrategy {

  @Override
  public boolean isPreemptionActive() {
    return true;
  }
  
  @Override
  public boolean canBePreempted(TaskStatus status) {
    return status.getPhase() == TaskStatus.Phase.REDUCE && 
           !this.isPreempted(status);
  }

  @Override
  public boolean canBeResumed(TaskStatus status) {
    return status.getPhase() == TaskStatus.Phase.REDUCE &&
           this.isPreempted(status);
  }

  @Override
  public boolean preempt(TaskInProgress tip, TaskStatus status) {
    return tip.suspendTaskAttempt(status.getTaskID());
  }

  @Override
  public boolean resume(TaskInProgress tip, TaskStatus status) {
    return tip.resumeTaskAttempt(status.getTaskID());
  }

  @Override
  public boolean isPreempted(TaskStatus status) {
    return status.getRunState() == State.SUSPENDED;
  }

  @Override
  public int getPreemptedTasks(JobInProgress jip, TaskType type) {
    return type == TaskType.MAP ? jip.getSuspendedMapTasks() 
                                : jip.getSuspendedReduceTasks();
  }


  @Override
  public String toString() {
    return "Suspend";
  }
}
