package org.apache.hadoop.mapred;

public enum PreemptionType {
  NONE, KILL, SUSP;
}
