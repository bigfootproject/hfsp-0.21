package org.apache.hadoop.conf;

public interface PrintConfiguration extends Configurable {
  public void printConfiguration();
}
